package uz.pdp.appmfactorpraktikaserver.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import uz.pdp.appmfactorpraktikaserver.controller.AuthController;
import uz.pdp.appmfactorpraktikaserver.entity.Role;
import uz.pdp.appmfactorpraktikaserver.entity.User;
import uz.pdp.appmfactorpraktikaserver.entity.enums.RoleName;
import uz.pdp.appmfactorpraktikaserver.exceptions.ResourceNotFound;
import uz.pdp.appmfactorpraktikaserver.payload.ApiResponse;
import uz.pdp.appmfactorpraktikaserver.payload.ReqSms;
import uz.pdp.appmfactorpraktikaserver.payload.ReqUser;
import uz.pdp.appmfactorpraktikaserver.repo.RoleRepository;
import uz.pdp.appmfactorpraktikaserver.repo.UserRepository;
import uz.pdp.appmfactorpraktikaserver.service.TwilioService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static uz.pdp.appmfactorpraktikaserver.entity.enums.RoleName.ROLE_SUPER_ADMIN;

@Service
public class AuthService implements UserDetailsService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final RoleRepository roleRepository;

    @Autowired
    TwilioService twilioService;

    @Autowired
    AuthController authController;


    public AuthService(@Lazy UserRepository userRepository, PasswordEncoder passwordEncoder, RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.roleRepository = roleRepository;
    }


    @Override
    public UserDetails loadUserByUsername(String phoneNumber) throws UsernameNotFoundException {
        return (UserDetails) userRepository.findByPhoneNumber(phoneNumber).orElseThrow(() -> new UsernameNotFoundException(phoneNumber));
    }

    public UserDetails loadUserById(UUID userId) {
        return (UserDetails) userRepository.findById(userId).orElseThrow(() -> new UsernameNotFoundException("User id not found: " + userId));
    }


    public ApiResponse saveUser(ReqUser reqUser) {

            try {
                User user = new User();
                user.setFirstName(reqUser.getFirstName());
                user.setLastName(reqUser.getLastName());
                user.setPassword(passwordEncoder.encode(reqUser.getPassword()));
                user.setPhoneNumber(reqUser.getPhoneNumber());
                user.setRoles(roleRepository.findAllByNameIn(
                        Collections.singletonList(RoleName.ROLE_CUSTOMER)));
//            twilioService.sendSms(reqUser.getPhoneNumber());
                userRepository.save(user);
                return new ApiResponse("success", true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        return new ApiResponse("error", false, reqUser);
    }

    public ApiResponse editUser(ReqUser reqUser) {

        try {
            User user = userRepository.findById(reqUser.getId()).orElseThrow(() -> new ResourceNotFound("User", "id", reqUser.getId()));
            user.setFirstName(reqUser.getFirstName());
            user.setLastName(reqUser.getLastName());
            user.setPhoneNumber(reqUser.getPhoneNumber());
            user.setPassword(reqUser.getPassword());
            user.setCompanyName(reqUser.getCompanyName());
            user.setProfession(reqUser.getProfession());
            user.setRegion(reqUser.getRegion());
            user.setBirthDate(reqUser.getBirthDate());
            userRepository.save(user);
            return new ApiResponse("success", true);

        } catch (ResourceNotFound resourceNotFound) {
            resourceNotFound.printStackTrace();
        }
        return new ApiResponse("error", false);
    }

    public ApiResponse deleteUser(UUID userId) {
        try {
            userRepository.deleteById(userId);
            return new ApiResponse("success", true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ApiResponse("error", false);
    }

    public ApiResponse changeEnabled(UUID userId) {
        return new ApiResponse("User " + (userRepository.changeEnabledById(userId) ? "enabled" : "disabled"), true);
    }

}

