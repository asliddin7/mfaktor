package uz.pdp.appmfactorpraktikaserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppMfactorpraktikaServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppMfactorpraktikaServerApplication.class, args);
    }

}
