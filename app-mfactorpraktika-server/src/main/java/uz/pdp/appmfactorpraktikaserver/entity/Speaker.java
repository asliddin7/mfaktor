package uz.pdp.appmfactorpraktikaserver.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.appmfactorpraktikaserver.entity.template.AbsEntity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;
import java.io.File;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Speaker extends AbsEntity {

    @OneToOne(fetch = FetchType.LAZY)
    private Attachment picture;

    @OneToOne(fetch = FetchType.LAZY)
    private User user;
}
