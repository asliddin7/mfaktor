package uz.pdp.appmfactorpraktikaserver.entity.enums;

public enum RoleName {

    ROLE_SUPER_ADMIN,
    ROLE_ADMIN,
    ROLE_CUSTOMER,
    ROLE_SPEAKER

}
