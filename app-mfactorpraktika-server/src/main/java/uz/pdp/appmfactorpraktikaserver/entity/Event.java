package uz.pdp.appmfactorpraktikaserver.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.appmfactorpraktikaserver.entity.template.AbsEntity;

import javax.persistence.*;
import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Event extends AbsEntity {

    private String name;

    private Date date;

    private String price;

    @ManyToOne
    private Speaker speaker;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "event_seat", joinColumns = {@JoinColumn(name = "event_id")},
            inverseJoinColumns = {@JoinColumn(name = "sent_id")})
    private List<Seat> seats;

    @OneToOne(fetch = FetchType.LAZY)
    private Attachment picture;
}
