package uz.pdp.appmfactorpraktikaserver.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.appmfactorpraktikaserver.entity.template.AbsEntity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AttachmentContent extends AbsEntity {

    private byte[] bytes;

    @OneToOne(fetch = FetchType.LAZY)
    private Attachment attachment;
}
