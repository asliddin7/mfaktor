package uz.pdp.appmfactorpraktikaserver.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.appmfactorpraktikaserver.entity.template.AbsEntity;

import javax.persistence.Entity;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PhoneVerify extends AbsEntity {

    private String phoneNumber;
    private Integer verifyCode;
    private boolean isVerify;

}
