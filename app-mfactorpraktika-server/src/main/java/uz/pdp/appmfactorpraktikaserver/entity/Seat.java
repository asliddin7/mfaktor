package uz.pdp.appmfactorpraktikaserver.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.appmfactorpraktikaserver.entity.template.AbsEntity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Seat extends AbsEntity{

    private Integer number;

    private char letter;

    private double price;

    private String rowNumber;

    private boolean isBooked;
}
