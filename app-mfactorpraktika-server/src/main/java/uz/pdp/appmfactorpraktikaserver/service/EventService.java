package uz.pdp.appmfactorpraktikaserver.service;

import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.stereotype.Service;
        import uz.pdp.appmfactorpraktikaserver.entity.Event;
import uz.pdp.appmfactorpraktikaserver.entity.Seat;
import uz.pdp.appmfactorpraktikaserver.exceptions.ResourceNotFound;
import uz.pdp.appmfactorpraktikaserver.payload.ApiResponse;
        import uz.pdp.appmfactorpraktikaserver.payload.ReqEvent;
import uz.pdp.appmfactorpraktikaserver.repo.AttachmentRepository;
import uz.pdp.appmfactorpraktikaserver.repo.EventRepo;
import uz.pdp.appmfactorpraktikaserver.repo.SeatRepo;
import uz.pdp.appmfactorpraktikaserver.repo.SpeakerRepo;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class EventService {

    @Autowired
    EventRepo eventRepo;

    @Autowired
    SeatRepo seatRepo;

    @Autowired
    SpeakerRepo speakerRepo;

    @Autowired
    AttachmentRepository attachmentRepository;

    public ApiResponse saveEvent (ReqEvent reqEvent){
        try {
            Event event=new Event();
            event.setName(reqEvent.getName());
            event.setDate(reqEvent.getDate());
            if (reqEvent.getSpeakerId()!=null){
                event.setSpeaker(speakerRepo.findById(reqEvent.getSpeakerId()).orElseThrow(() -> new ResourceNotFound("Speaker","id",reqEvent.getSpeakerId())));
            }
            List<Seat> seats = new ArrayList<>();
            for (UUID seatId : reqEvent.getSeatIds()) {
                seats.add(seatRepo.findById(seatId).orElseThrow(() -> new ResourceNotFound("Seat", "id", reqEvent.getSeatIds())));
            }
            event.setSeats(seats);
            if (reqEvent.getAttachmentId()!=null){
                event.setPicture(attachmentRepository.findById(reqEvent.getAttachmentId()).orElseThrow(() -> new ResourceNotFound("Attachment","id",reqEvent.getAttachmentId())));
            }

            eventRepo.save(event);
            return new ApiResponse("success",true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ApiResponse("error",false);
    }

    public ApiResponse editEvent(ReqEvent reqEvent){
        Event event = eventRepo.findById(reqEvent.getAttachmentId()).orElseThrow(() -> new ResourceNotFound("Event", "id", reqEvent.getAttachmentId()));
        event.setDate(reqEvent.getDate());
        if (reqEvent.getSpeakerId()!=null){
            event.setSpeaker(speakerRepo.findById(reqEvent.getSpeakerId()).orElseThrow(() -> new ResourceNotFound("Speaker","id",reqEvent.getSpeakerId())));
        }
        List<Seat> seats = new ArrayList<>();
        for (UUID seatId : reqEvent.getSeatIds()) {
            seats.add(seatRepo.findById(seatId).orElseThrow(() -> new ResourceNotFound("Seat", "id", reqEvent.getSeatIds())));
        }
        event.setSeats(seats);
        if (reqEvent.getAttachmentId()!=null){
            event.setPicture(attachmentRepository.findById(reqEvent.getAttachmentId()).orElseThrow(() -> new ResourceNotFound("Attachment","id",reqEvent.getAttachmentId())));
        }
        eventRepo.save(event);
        return new ApiResponse("success",true);
    }

    public ApiResponse deleteEvent(UUID eventId){
        try {
            eventRepo.deleteById(eventId);
            return new ApiResponse("success",true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ApiResponse("error",false);
    }

}
