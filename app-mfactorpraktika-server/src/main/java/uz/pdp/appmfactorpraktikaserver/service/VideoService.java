package uz.pdp.appmfactorpraktikaserver.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.appmfactorpraktikaserver.entity.Video;
import uz.pdp.appmfactorpraktikaserver.exceptions.ResourceNotFound;
import uz.pdp.appmfactorpraktikaserver.payload.ApiResponse;
import uz.pdp.appmfactorpraktikaserver.payload.ReqVideo;
import uz.pdp.appmfactorpraktikaserver.repo.VideoRepository;

import java.util.UUID;

@Service
public class VideoService {

    @Autowired
    VideoRepository videoRepository;

    public ApiResponse saveVideo(ReqVideo reqVideo){
        try {
            Video video= new Video();
            video.setLink(reqVideo.getLink());
            video.setType(reqVideo.getType());
            videoRepository.save(video);
            return new ApiResponse("success",true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ApiResponse("error",false);
    }

    public ApiResponse editVideo(ReqVideo reqVideo){
        try {
            videoRepository.findById(reqVideo.getId()).orElseThrow(() -> new ResourceNotFound("Video","id",reqVideo.getId()));
            Video video= new Video();
            video.setLink(reqVideo.getLink());
            video.setType(reqVideo.getType());
            videoRepository.save(video);
            return new ApiResponse("success",true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ApiResponse("error",false);
    }

    public ApiResponse deleteVideo(UUID videoId){
        try {
            videoRepository.deleteById(videoId);
            return new ApiResponse("success",true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ApiResponse("error",false);
    }
}
