package uz.pdp.appmfactorpraktikaserver.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import uz.pdp.appmfactorpraktikaserver.entity.Attachment;
import uz.pdp.appmfactorpraktikaserver.entity.AttachmentContent;
import uz.pdp.appmfactorpraktikaserver.exceptions.ResourceNotFound;
import uz.pdp.appmfactorpraktikaserver.payload.ApiResponse;
import uz.pdp.appmfactorpraktikaserver.payload.ResUploadFile;
import uz.pdp.appmfactorpraktikaserver.repo.AttachmentContentRepository;
import uz.pdp.appmfactorpraktikaserver.repo.AttachmentRepository;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

@Service
public class AttachmentService {

    @Autowired
    AttachmentRepository attachmentRepository;

    @Autowired
    AttachmentContentRepository attachmentContentRepository;

public ApiResponse saveFile(MultipartFile request) throws IOException {
    Attachment attachment = attachmentRepository.save(new Attachment(request.getName(), request.getContentType(), request.getSize()));
    attachmentContentRepository.save(new AttachmentContent(request.getBytes(),attachment));
//    Iterator<String> itr = request.getFileNames();
//    MultipartFile file;
//    List<ResUploadFile> resUploadFiles = new ArrayList<>();
//    while (itr.hasNext()) {
//        file = request.getFile(itr.next());
//        Attachment attachment;
//        try{
//            Attachment attachment1=new Attachment(file.getOriginalFilename(),file.getContentType(), file.getSize());
//            attachment= attachmentRepository.save(attachment1);
//        }
//        catch (Exception e){
//            attachment = attachmentRepository.save(new Attachment(file.getOriginalFilename(), file.getContentType(), file.getSize()));
//
//        }
//        try {
//            attachmentContentRepository.save(new AttachmentContent( file.getBytes(),attachment));
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        resUploadFiles.add(new ResUploadFile(attachment.getId(),
//                attachment.getName(),
//                ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/file/").path(attachment.getId().toString()).toUriString(),
//                attachment.getSize()));
//    }
    return new ApiResponse("sucess", true,attachment);
}

    public HttpEntity<?> getAttachmentContent(UUID attachmentId, HttpServletResponse response) {
        Attachment attachment = attachmentRepository.findById(attachmentId).orElseThrow(() -> new ResourceNotFound("Attachment", "id", attachmentId));
        AttachmentContent attachmentContent = attachmentContentRepository.findByAttachment(attachment).orElseThrow(() -> new ResourceNotFound("Attachment content", "attachment id", attachmentId));
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(attachment.getContentType()))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + attachment.getName() + "\"")
                .body(attachmentContent.getBytes());
    }

    public ApiResponse deleteFile(UUID id) {
        try {
            attachmentContentRepository.deleteById(id);
            return new ApiResponse("success",true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ApiResponse("error",false);
    }

    public ApiResponse editFile(MultipartHttpServletRequest request) {
        return null;
    }

    public ApiResponse getAll(List<UUID> attachmentIds) {
        try {
            List<AttachmentContent> allById = attachmentContentRepository.findAllById(attachmentIds);
            return new ApiResponse("success",true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ApiResponse("error",false);
    }
}
