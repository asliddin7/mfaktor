package uz.pdp.appmfactorpraktikaserver.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.appmfactorpraktikaserver.entity.Speaker;
import uz.pdp.appmfactorpraktikaserver.exceptions.ResourceNotFound;
import uz.pdp.appmfactorpraktikaserver.payload.ApiResponse;
import uz.pdp.appmfactorpraktikaserver.payload.ReqSpeaker;
import uz.pdp.appmfactorpraktikaserver.repo.AttachmentRepository;
import uz.pdp.appmfactorpraktikaserver.repo.SpeakerRepo;
import uz.pdp.appmfactorpraktikaserver.repo.UserRepository;

import java.util.UUID;

@Service
public class SpeakerService {

    @Autowired
    SpeakerRepo speakerRepo;

    @Autowired
    UserRepository userRepository;

    @Autowired
    AttachmentRepository attachmentRepository;

    public ApiResponse saveSpeaker(ReqSpeaker reqSpeaker) {
        try {
            Speaker speaker=new Speaker();
            if (reqSpeaker.getUserId()!=null){
                speaker.setUser(userRepository.findById(reqSpeaker.getUserId()).orElseThrow(() -> new ResourceNotFound("User","id",reqSpeaker.getUserId())));
            }
            if (reqSpeaker.getAttachmentId()!=null){
                speaker.setPicture(attachmentRepository.findById(reqSpeaker.getAttachmentId()).orElseThrow(() -> new ResourceNotFound("Attachment","id",reqSpeaker.getAttachmentId())));
            }
            speakerRepo.save(speaker);
            return new ApiResponse("success",true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ApiResponse("success",true);
    }

    public ApiResponse editSpeaker(ReqSpeaker reqSpeaker) {
        Speaker speaker=speakerRepo.findById(reqSpeaker.getId()).orElseThrow(() -> new ResourceNotFound("Speaker","id",reqSpeaker.getId()));
        if (reqSpeaker.getUserId()!=null){
            speaker.setUser(userRepository.findById(reqSpeaker.getUserId()).orElseThrow(() -> new ResourceNotFound("user","id",reqSpeaker.getUserId())));
        }
         if (reqSpeaker.getAttachmentId()!=null){
                speaker.setPicture(attachmentRepository.findById(reqSpeaker.getAttachmentId()).orElseThrow(() -> new ResourceNotFound("Attachment","id",reqSpeaker.getAttachmentId())));
            }
        speakerRepo.save(speaker);
        return new ApiResponse("success",true);
    }

    public ApiResponse deleteSpeaker(UUID speakerId) {
        try {
            speakerRepo.deleteById(speakerId);
            return new ApiResponse("success",true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ApiResponse("error",false);
    }
}
