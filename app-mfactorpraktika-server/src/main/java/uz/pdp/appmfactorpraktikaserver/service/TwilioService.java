package uz.pdp.appmfactorpraktikaserver.service;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import uz.pdp.appmfactorpraktikaserver.entity.PhoneVerify;
import uz.pdp.appmfactorpraktikaserver.payload.ApiResponse;
import uz.pdp.appmfactorpraktikaserver.payload.ReqSms;
import uz.pdp.appmfactorpraktikaserver.repo.PhoneVerifyRepository;

import java.util.Optional;
import java.util.Random;

@Service
public class TwilioService {

    @Autowired
    PhoneVerifyRepository phoneVerifyRepository;

    @Value("AC444a03aa2efe5b5a8dc0657b44809a81")
    private String accountSid;

    @Value("b898ae3cc6c8a9a739c38d17cf8f1042")
    private String authToken;

    @Value("+14405177408")
    private String trialPhoneNumber;

    public ApiResponse sendSms(String phoneNumber){
        try {
            String checkedPhoneNumber = (phoneNumber.startsWith("+")?phoneNumber.replace(" ",""):"+"+phoneNumber.replace(" ",""));
            Twilio.init(accountSid,authToken);
            int code = generateCode();
            Message message = Message.creator(new PhoneNumber(checkedPhoneNumber),new PhoneNumber(trialPhoneNumber),"Your verification code: "+code).create();
            Optional<PhoneVerify> byPhoneNumber = phoneVerifyRepository.findByPhoneNumber(phoneNumber);
            if (byPhoneNumber.isPresent()){
                PhoneVerify phoneVerify = byPhoneNumber.get();
                phoneVerify.setVerifyCode(code);
                phoneVerify.setVerify(false);
                phoneVerifyRepository.save(phoneVerify);
            }else {
                PhoneVerify phoneVerify = new PhoneVerify();
                phoneVerify.setPhoneNumber(phoneNumber);
                phoneVerify.setVerifyCode(code);
                phoneVerify.setVerify(false);
                phoneVerifyRepository.save(phoneVerify);
            }
            return new ApiResponse("successfully",true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ApiResponse("error",false);
    }

    public int generateCode(){
        return new Random().nextInt(900000)+100000;
    }


    public ApiResponse checkSms(ReqSms reqSms){
        Optional<PhoneVerify> byPhoneNumber = phoneVerifyRepository.findByPhoneNumber(reqSms.getPhoneNumber());
        if (byPhoneNumber.isPresent()){
            PhoneVerify phoneVerify = byPhoneNumber.get();
            if (phoneVerify.getVerifyCode()==reqSms.getVerifyCode()){
                phoneVerify.setVerify(true);
                phoneVerifyRepository.save(phoneVerify);
                return new ApiResponse("success",true);
            }else {
                return new ApiResponse("error",false);
            }
        }else {
            return new ApiResponse("ERROR",false);
        }
    }
}
