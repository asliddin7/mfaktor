package uz.pdp.appmfactorpraktikaserver.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.appmfactorpraktikaserver.entity.Seat;
import uz.pdp.appmfactorpraktikaserver.exceptions.ResourceNotFound;
import uz.pdp.appmfactorpraktikaserver.payload.ApiResponse;
import uz.pdp.appmfactorpraktikaserver.payload.ReqSeat;
import uz.pdp.appmfactorpraktikaserver.repo.SeatRepo;

import java.util.List;
import java.util.UUID;

@Service
public class SeatService {

    @Autowired
    SeatRepo seatRepo;

    public ApiResponse saveSeat(ReqSeat reqSeat) {
        try {
            Seat seat=new Seat();
            seat.setNumber(reqSeat.getNumber());
            seat.setLetter(reqSeat.getLetter());
            seat.setPrice(reqSeat.getPrice());
            seat.setRowNumber(reqSeat.getRowNumber());
            seat.setBooked(false);
            seatRepo.save(seat);
            return new ApiResponse("success", true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ApiResponse("error", false);

    }

    public ApiResponse bookedSeat(List<UUID> seatIds){
        try {
            List<Seat> allById = seatRepo.findAllById(seatIds);
            for (Seat seat : allById) {
                seat.setBooked(true);
                seatRepo.save(seat);
            }
            return new ApiResponse("success", true);
        } catch (ResourceNotFound resourceNotFound) {
            resourceNotFound.printStackTrace();
        }
        return new ApiResponse("error", false);

    }

    public ApiResponse editSeat(ReqSeat reqSeat) {
        Seat seat=seatRepo.findById(reqSeat.getId()).orElseThrow(() -> new ResourceNotFound("Seat","id",reqSeat.getId()));
        seat.setNumber(reqSeat.getNumber());
        seat.setLetter(reqSeat.getLetter());
        seat.setPrice(reqSeat.getPrice());
        seat.setRowNumber(reqSeat.getRowNumber());
        seatRepo.save(seat);
        return new ApiResponse("success",true);
    }

    public ApiResponse deleteSeat(UUID seatId) {
        try {
            seatRepo.deleteById(seatId);
            return new ApiResponse("success",true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ApiResponse("error",false);
    }
}
