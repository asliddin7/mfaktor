package uz.pdp.appmfactorpraktikaserver.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.appmfactorpraktikaserver.entity.SoldSeat;
import uz.pdp.appmfactorpraktikaserver.exceptions.ResourceNotFound;
import uz.pdp.appmfactorpraktikaserver.payload.ApiResponse;
import uz.pdp.appmfactorpraktikaserver.payload.ReqSoldSeat;
import uz.pdp.appmfactorpraktikaserver.repo.EventRepo;
import uz.pdp.appmfactorpraktikaserver.repo.SoldSeatRepo;

import java.util.UUID;

@Service
public class SoldSeatService {

    @Autowired
    SoldSeatRepo soldSeatRepo;

    @Autowired
    EventRepo eventRepo;

    public ApiResponse saveSoldSeat(ReqSoldSeat reqSoldSeat) {
        try {
            SoldSeat soldSeat=new SoldSeat();
            soldSeat.setSeat(reqSoldSeat.getSeat());
            if (reqSoldSeat.getEventId()!=null){
                soldSeat.setEvent(eventRepo.findById(reqSoldSeat.getEventId()).orElseThrow(() -> new ResourceNotFound("Event","id",reqSoldSeat.getEventId())));
            }
            soldSeat.setPrice(reqSoldSeat.getPrice());
            soldSeatRepo.save(soldSeat);
            return new ApiResponse("success",true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ApiResponse("error",false);
    }

    public ApiResponse editSoldSeat(ReqSoldSeat reqSoldSeat) {
        SoldSeat soldSeat=soldSeatRepo.findById(reqSoldSeat.getId()).orElseThrow(() -> new ResourceNotFound("SoldSeat","id",reqSoldSeat.getId()));
        soldSeat.setSeat(reqSoldSeat.getSeat());
        if (reqSoldSeat.getEventId()!=null){
            soldSeat.setEvent(eventRepo.findById(reqSoldSeat.getEventId()).orElseThrow(() -> new ResourceNotFound("Event","id",reqSoldSeat.getEventId())));
        }
        soldSeat.setPrice(reqSoldSeat.getPrice());
        soldSeatRepo.save(soldSeat);
        return new ApiResponse("success",true);
    }

    public ApiResponse deleteSoldSeat(UUID soldSeatId) {
        try {
            soldSeatRepo.deleteById(soldSeatId);
            return new ApiResponse("success",true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ApiResponse("success",true);
    }
}
