package uz.pdp.appmfactorpraktikaserver.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import uz.pdp.appmfactorpraktikaserver.entity.Role;
import uz.pdp.appmfactorpraktikaserver.entity.User;
import uz.pdp.appmfactorpraktikaserver.entity.enums.RoleName;
import uz.pdp.appmfactorpraktikaserver.repo.RoleRepository;
import uz.pdp.appmfactorpraktikaserver.repo.UserRepository;


import java.util.Arrays;
import java.util.Collections;

import static uz.pdp.appmfactorpraktikaserver.entity.enums.RoleName.*;


@Component
public class DataLoader implements CommandLineRunner {

    @Value("${spring.datasource.initialization-mode}")
    private String initialMode;


    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Override
    public void run(String... args) throws Exception {
        if (initialMode.equals("always")) {
            roleRepository.save(new Role(1, ROLE_CUSTOMER));
            roleRepository.save(new Role(2, ROLE_SPEAKER));
            roleRepository.save(new Role(3, ROLE_ADMIN));
            roleRepository.save(new Role(4, ROLE_SUPER_ADMIN));
            userRepository.save(new User(
                    "Asliddin",
                    "Muhiddinov",
                    "+998975960530",
                    passwordEncoder.encode("0530"),
                    roleRepository.findAllByNameIn(
                            Collections.singletonList(ROLE_SUPER_ADMIN))));

        }
    }

}
