package uz.pdp.appmfactorpraktikaserver.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.appmfactorpraktikaserver.payload.ApiResponse;
import uz.pdp.appmfactorpraktikaserver.payload.ReqEvent;
import uz.pdp.appmfactorpraktikaserver.repo.EventRepo;
import uz.pdp.appmfactorpraktikaserver.service.EventService;

import java.util.UUID;

@RestController
@RequestMapping("api/event")
public class EventController {

    @Autowired
    EventRepo eventRepo;
    @Autowired
    EventService eventService;


    @GetMapping("/getEvent")
    public HttpEntity<?> getEvent(){
        return ResponseEntity.ok(eventRepo.findAll());
    }

    @PostMapping("/saveEvent")
    public HttpEntity<?> saveEvent(@RequestBody ReqEvent reqEvent){
        ApiResponse apiResponse= eventService.saveEvent(reqEvent);
        return ResponseEntity.status(apiResponse.isSuccess() ? HttpStatus.OK : HttpStatus.CONFLICT).body(apiResponse);
    }

    @PutMapping("/editEvent")
    public HttpEntity<?> editEvent(@RequestBody ReqEvent reqEvent){
        ApiResponse apiResponse=eventService.editEvent(reqEvent);
        return ResponseEntity.status(apiResponse.isSuccess()?HttpStatus.OK : HttpStatus.CONFLICT).body(apiResponse);
    }

    @DeleteMapping("/deleteEvent")
    public HttpEntity<?> deleteEvent(@RequestParam UUID eventId){
        ApiResponse apiResponse=eventService.deleteEvent(eventId);
        return ResponseEntity.status(apiResponse.isSuccess()? HttpStatus.OK : HttpStatus.CONFLICT).body(apiResponse);
    }

}
