package uz.pdp.appmfactorpraktikaserver.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.appmfactorpraktikaserver.payload.ApiResponse;
import uz.pdp.appmfactorpraktikaserver.payload.ReqSoldSeat;
import uz.pdp.appmfactorpraktikaserver.repo.SoldSeatRepo;
import uz.pdp.appmfactorpraktikaserver.service.SoldSeatService;

import java.util.UUID;

@RestController
@RequestMapping("/api/soldSeat")
public class SoldSeatController {

    @Autowired
    SoldSeatRepo soldSeatRepo;

    @Autowired
    SoldSeatService soldSeatService;

    @GetMapping("/getAllSoldSeats")
    public HttpEntity<?> getAllSoldSeats(){
        return ResponseEntity.ok(soldSeatRepo.findAll());
    }

    @PostMapping("/saveSoldSeat")
    public HttpEntity<?> saveSoldSeat(@RequestBody ReqSoldSeat reqSoldSeat){
        ApiResponse apiResponse=soldSeatService.saveSoldSeat(reqSoldSeat);
        return ResponseEntity.status(apiResponse.isSuccess() ? HttpStatus.OK : HttpStatus.CONFLICT).body(apiResponse);
    }

    @PutMapping("/editSoldSeat")
    public HttpEntity<?> editSoldSeat(@RequestBody ReqSoldSeat reqSoldSeat){
        ApiResponse apiResponse=soldSeatService.editSoldSeat(reqSoldSeat);
        return ResponseEntity.status(apiResponse.isSuccess() ? HttpStatus.OK : HttpStatus.CONFLICT).body(apiResponse);
    }

    @DeleteMapping("/deleteSoldSeat")
    public HttpEntity<?> deleteSoldSeat (@RequestParam UUID soldSeatId){
        ApiResponse apiResponse=soldSeatService.deleteSoldSeat(soldSeatId);
        return ResponseEntity.status(apiResponse.isSuccess()? HttpStatus.OK : HttpStatus.CONFLICT).body(apiResponse);
    }
}
