package uz.pdp.appmfactorpraktikaserver.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import uz.pdp.appmfactorpraktikaserver.payload.ApiResponse;
import uz.pdp.appmfactorpraktikaserver.service.AttachmentService;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/file")
public class AttachmentController {

    @Autowired
    AttachmentService attachmentService;

    @GetMapping("/{id}")
    public HttpEntity<?> getFile(@PathVariable UUID id, HttpServletResponse response) {
        return attachmentService.getAttachmentContent(id, response);
    }
    @GetMapping("/getAll")
    public HttpEntity<?> getAll(@RequestParam List<UUID> attachmentIds){
        ApiResponse apiResponse=attachmentService.getAll(attachmentIds);
        return ResponseEntity.status(apiResponse.isSuccess() ? HttpStatus.OK : HttpStatus.CONFLICT).body(apiResponse);
    }

    @PostMapping("/saveFile")
    public HttpEntity<?> saveFile(@ModelAttribute MultipartFile request) throws IOException {
        ApiResponse apiResponse = attachmentService.saveFile(request);
        return ResponseEntity.status(apiResponse.isSuccess() ? HttpStatus.OK : HttpStatus.CONFLICT).body(apiResponse);
    }

    @PutMapping("/editFile")
    public HttpEntity<?> editFile(MultipartHttpServletRequest request){
        ApiResponse apiResponse=attachmentService.editFile(request);
        return ResponseEntity.status(apiResponse.isSuccess() ? HttpStatus.OK : HttpStatus.CONFLICT).body(apiResponse);
    }


    @DeleteMapping("/deleteFile")
    public HttpEntity<?> deleteFile(@RequestParam UUID id){
        ApiResponse apiResponse=attachmentService.deleteFile(id);
        return ResponseEntity.status(apiResponse.isSuccess()? HttpStatus.OK : HttpStatus.CONFLICT).body(apiResponse);
    }
}
