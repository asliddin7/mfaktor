package uz.pdp.appmfactorpraktikaserver.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.appmfactorpraktikaserver.payload.ApiResponse;
import uz.pdp.appmfactorpraktikaserver.payload.ReqSpeaker;
import uz.pdp.appmfactorpraktikaserver.repo.SpeakerRepo;
import uz.pdp.appmfactorpraktikaserver.service.SpeakerService;

import java.util.UUID;

@RestController
@RequestMapping("/api/speaker")
public class SpeakerController {
    @Autowired
    SpeakerRepo speakerRepo;

    @Autowired
    SpeakerService speakerService;

    @GetMapping("/getAllSpeakers")
    public HttpEntity<?> getAllSpeakers(){
        return ResponseEntity.ok(speakerRepo.findAll());
    }

    @PostMapping("/saveSpeaker")
    public HttpEntity<?> saveSpeaker(@RequestBody ReqSpeaker reqSpeaker){
        ApiResponse apiResponse=speakerService.saveSpeaker(reqSpeaker);
        return ResponseEntity.status(apiResponse.isSuccess() ? HttpStatus.OK : HttpStatus.CONFLICT).body(apiResponse);
    }

    @PutMapping("/editSpeaker")
    public HttpEntity<?> editSpeaker(@RequestBody ReqSpeaker reqSpeaker){
        ApiResponse apiResponse=speakerService.editSpeaker(reqSpeaker);
        return ResponseEntity.status(apiResponse.isSuccess() ? HttpStatus.OK : HttpStatus.CONFLICT).body(apiResponse);
    }
    @DeleteMapping("/deleteSpeaker")
    public HttpEntity<?> deleteSpeakear(@RequestParam UUID speakerId){
        ApiResponse apiResponse=speakerService.deleteSpeaker(speakerId);
        return ResponseEntity.status(apiResponse.isSuccess() ? HttpStatus.OK : HttpStatus.CONFLICT).body(apiResponse);
    }
}
