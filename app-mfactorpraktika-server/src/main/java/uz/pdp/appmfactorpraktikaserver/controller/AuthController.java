package uz.pdp.appmfactorpraktikaserver.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import uz.pdp.appmfactorpraktikaserver.entity.User;
import uz.pdp.appmfactorpraktikaserver.payload.ApiResponse;
import uz.pdp.appmfactorpraktikaserver.payload.JwtResponse;
import uz.pdp.appmfactorpraktikaserver.payload.ReqSignIn;
import uz.pdp.appmfactorpraktikaserver.payload.ReqUser;
import uz.pdp.appmfactorpraktikaserver.repo.UserRepository;
import uz.pdp.appmfactorpraktikaserver.security.AuthService;
import uz.pdp.appmfactorpraktikaserver.security.CurrentUser;
import uz.pdp.appmfactorpraktikaserver.security.JwtTokenProvider;
import uz.pdp.appmfactorpraktikaserver.service.TwilioService;

import java.util.List;
import java.util.Optional;
import java.util.UUID;


@RestController
@RequestMapping("/api/auth")
public class AuthController {
    @Autowired
    JwtTokenProvider jwtTokenProvider;

    @Autowired
    AuthenticationManager authenticate;

    @Autowired
    AuthService authService;

    @Autowired
    UserRepository userRepository;



    @PostMapping("/login")
    public HttpEntity<?> login(@RequestBody ReqSignIn reqSignIn) {
        return ResponseEntity.ok(getApiToken(reqSignIn.getPhoneNumber(), reqSignIn.getPassword()));
    }

    public HttpEntity<?> getApiToken(String phoneNumber, String password) {
        Authentication authentication = authenticate.authenticate(
                new UsernamePasswordAuthenticationToken(phoneNumber, password)
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtTokenProvider.generateToken(authentication);
        return ResponseEntity.ok(new JwtResponse(jwt));
    }


    @GetMapping
    public HttpEntity<?> getAll() {
        List<User> all = userRepository.findAll();
        return ResponseEntity.ok(all);
    }

    @GetMapping("/getUserByPhone")
    public HttpEntity<?> getUser(@RequestParam String phone) {
        phone = phone.trim();
        phone = phone.startsWith("+") ? phone : "+" + phone;
        Optional<User> byPhoneNumber = userRepository.findByPhoneNumber(phone);
        return ResponseEntity.status(200).body(byPhoneNumber.isPresent() ? byPhoneNumber.get() : "null");
    }

    @GetMapping("/me")
    public HttpEntity<?> userMe(@CurrentUser User user){
        return ResponseEntity.status(user!=null?200:409).body(user);
    }


    @PostMapping("/saveUser")
    public HttpEntity<?> saveUser(@RequestBody ReqUser reqUser) {
        ApiResponse apiResponse = authService.saveUser(reqUser);
        if (apiResponse.isSuccess()){
            return getApiToken(reqUser.getPhoneNumber(),reqUser.getPassword());
        }
        return ResponseEntity.status(409).body(apiResponse);
    }

    @PutMapping("/editUser")
    public HttpEntity<?> editUser(@RequestBody ReqUser reqUser) {
        ApiResponse apiResponse = authService.editUser(reqUser);
        return ResponseEntity.status(apiResponse.isSuccess() ? HttpStatus.OK : HttpStatus.CONFLICT).body(apiResponse);
    }

    @DeleteMapping("/deleteUser")
    public HttpEntity<?> deleteUser(@RequestParam UUID userId) {
        ApiResponse apiResponse = authService.deleteUser(userId);
        return ResponseEntity.status(apiResponse.isSuccess() ? HttpStatus.OK : HttpStatus.CONFLICT).body(apiResponse);
    }

    @PutMapping("/changeEnabled")
    public HttpEntity<?> changeEnabled(@RequestParam UUID userId) {
        ApiResponse apiResponse = authService.changeEnabled(userId);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200:409).body(apiResponse);
    }
}
