package uz.pdp.appmfactorpraktikaserver.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.appmfactorpraktikaserver.payload.ApiResponse;
import uz.pdp.appmfactorpraktikaserver.payload.ReqVideo;
import uz.pdp.appmfactorpraktikaserver.repo.VideoRepository;
import uz.pdp.appmfactorpraktikaserver.repo.VideoTypeRepository;
import uz.pdp.appmfactorpraktikaserver.service.VideoService;

import java.util.UUID;

@RestController
@RequestMapping("/api/video")
public class VideoController {

    @Autowired
    VideoRepository videoRepository;

    @Autowired
    VideoService videoService;

    @Autowired
    VideoTypeRepository videoTypeRepository;

    @GetMapping("/getAllVideoTypes")
    public HttpEntity<?> getVideoTypes() {
        return ResponseEntity.ok(videoTypeRepository.findAll());
    }

    @GetMapping("/getVideosByVideoType")
    public HttpEntity<?> getVideosByVideoType(@RequestParam UUID id) {
        return ResponseEntity.ok(videoRepository.findAllByTypeId(id));
    }
    @GetMapping("/searchVideoTypes")
    public HttpEntity<?> searchVideoTypes(@RequestParam String type){
    return ResponseEntity.ok(videoTypeRepository.findAllByTypeContainingIgnoreCase(type));
    }

    @GetMapping("/getVideo")
    public HttpEntity<?> getVideo() {
        return ResponseEntity.ok(videoRepository.findAll());
    }

    @PostMapping("/saveVideo")
    public HttpEntity<?> saveVideo(@RequestBody ReqVideo reqVideo) {
        ApiResponse  apiResponse = videoService.saveVideo(reqVideo);
        return ResponseEntity.status(apiResponse.isSuccess() ? HttpStatus.OK : HttpStatus.CONFLICT).body(apiResponse);
    }

    @PutMapping("/editVideo")
    public HttpEntity<?> editVideo(@RequestBody ReqVideo reqVideo) {
        ApiResponse apiResponse = videoService.editVideo(reqVideo);
        return ResponseEntity.status(apiResponse.isSuccess() ? HttpStatus.OK : HttpStatus.CONFLICT).body(apiResponse);
    }

    @DeleteMapping("/deleteVideo")
    public HttpEntity<?> deleteVideo(@RequestParam UUID videoId) {
        ApiResponse apiResponse = videoService.deleteVideo(videoId);
        return ResponseEntity.status(apiResponse.isSuccess() ? HttpStatus.OK : HttpStatus.CONFLICT).body(apiResponse);
    }
}
