package uz.pdp.appmfactorpraktikaserver.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.appmfactorpraktikaserver.payload.ApiResponse;
import uz.pdp.appmfactorpraktikaserver.payload.ReqSeat;
import uz.pdp.appmfactorpraktikaserver.repo.SeatRepo;
import uz.pdp.appmfactorpraktikaserver.service.SeatService;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/seat")
public class SeatController {

    @Autowired
    SeatRepo seatRepo;

    @Autowired
    SeatService seatService;

    @GetMapping("/getAllSeats")
    public HttpEntity<?> getAllSeats(){
        return ResponseEntity.ok(seatRepo.findAll());
    }

    @PostMapping("/saveSeat")
    public HttpEntity<?> saveSeat(@RequestBody ReqSeat reqSeat){
        ApiResponse apiResponse= seatService.saveSeat(reqSeat);
        return ResponseEntity.status(apiResponse.isSuccess() ? HttpStatus.OK : HttpStatus.CONFLICT).body(apiResponse);
    }
    @GetMapping("/bookedSeat")
    public HttpEntity<?> bookedSeat(@RequestParam List<UUID> seatIds){
    ApiResponse apiResponse=seatService.bookedSeat(seatIds);
    return ResponseEntity.status(apiResponse.isSuccess() ? HttpStatus.OK : HttpStatus.CONFLICT).body(apiResponse);
    }

    @PutMapping("/editSeat")
    public HttpEntity<?> editSeat(@RequestBody ReqSeat reqSeat){
        ApiResponse apiResponse=seatService.editSeat(reqSeat);
        return ResponseEntity.status(apiResponse.isSuccess()?HttpStatus.OK : HttpStatus.CONFLICT).body(apiResponse);
    }

    @DeleteMapping("/deleteSeat")
    public HttpEntity<?> deleteSeat(@RequestParam UUID seatId){
        ApiResponse apiResponse=seatService.deleteSeat(seatId);
        return ResponseEntity.status(apiResponse.isSuccess()?HttpStatus.OK : HttpStatus.CONFLICT).body(apiResponse);
    }

}
