package uz.pdp.appmfactorpraktikaserver.payload;

import lombok.Data;
import uz.pdp.appmfactorpraktikaserver.entity.VideoType;

@Data
public class ReqVideoType {

    private VideoType type;
}
