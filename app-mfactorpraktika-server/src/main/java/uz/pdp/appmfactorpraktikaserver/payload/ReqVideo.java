package uz.pdp.appmfactorpraktikaserver.payload;


import lombok.Data;
import uz.pdp.appmfactorpraktikaserver.entity.VideoType;

import java.util.UUID;

@Data
public class ReqVideo {

    private UUID id;

    private String link;

    private VideoType type;
}
