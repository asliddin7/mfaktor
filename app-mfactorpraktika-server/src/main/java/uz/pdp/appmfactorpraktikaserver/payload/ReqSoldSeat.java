package uz.pdp.appmfactorpraktikaserver.payload;

import lombok.Data;
import uz.pdp.appmfactorpraktikaserver.entity.Event;
import uz.pdp.appmfactorpraktikaserver.entity.Seat;

import java.util.UUID;

@Data
public class ReqSoldSeat {
    private UUID id;
    private Seat seat;
    private UUID eventId;
    private double price;
}
