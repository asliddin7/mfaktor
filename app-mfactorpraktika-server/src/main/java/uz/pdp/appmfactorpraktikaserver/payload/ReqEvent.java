package uz.pdp.appmfactorpraktikaserver.payload;

import lombok.Data;
import uz.pdp.appmfactorpraktikaserver.entity.Attachment;
import uz.pdp.appmfactorpraktikaserver.entity.Seat;
import uz.pdp.appmfactorpraktikaserver.entity.Speaker;

import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Data
public class ReqEvent {

    private UUID id;
    private String name;
    private Date date;
    private String price;
    private UUID speakerId;
    private List<UUID> seatIds;
    private UUID attachmentId;
}
