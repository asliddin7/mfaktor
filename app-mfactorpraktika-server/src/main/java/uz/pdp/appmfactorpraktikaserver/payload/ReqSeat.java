package uz.pdp.appmfactorpraktikaserver.payload;

import lombok.Data;

import java.util.UUID;

@Data
public class ReqSeat {

    private UUID id;
    private Integer number;
    private char letter;
    private double price;
    private String rowNumber;
    private boolean isBooked;
}
