package uz.pdp.appmfactorpraktikaserver.payload;

import lombok.Data;
import uz.pdp.appmfactorpraktikaserver.entity.User;

import java.io.File;
import java.util.UUID;

@Data
public class ReqSpeaker {
    private UUID id;
    private File picture;
    private UUID userId;
    private UUID AttachmentId;
}
