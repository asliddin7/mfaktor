package uz.pdp.appmfactorpraktikaserver.payload;

import lombok.Data;
import org.w3c.dom.stylesheets.LinkStyle;
import uz.pdp.appmfactorpraktikaserver.entity.Role;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Data
public class ReqUser {

    private UUID id;

    private String firstName;

    private String lastName;

    private String password;

    private String phoneNumber;

    private Date birthDate;
    private String profession;
    private String companyName;
    private String region;

}

