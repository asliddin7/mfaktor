package uz.pdp.appmfactorpraktikaserver.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.appmfactorpraktikaserver.entity.Seat;

import java.util.UUID;

public interface SeatRepo extends JpaRepository<Seat, UUID> {
}
