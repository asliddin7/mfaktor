package uz.pdp.appmfactorpraktikaserver.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.appmfactorpraktikaserver.entity.Video;

import java.util.List;
import java.util.UUID;

public interface VideoRepository extends JpaRepository<Video, UUID> {
    List<Video> findAllByTypeId(UUID type_id);
}
