package uz.pdp.appmfactorpraktikaserver.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.appmfactorpraktikaserver.entity.SoldSeat;

import java.util.UUID;

public interface SoldSeatRepo extends JpaRepository<SoldSeat, UUID> {

}
