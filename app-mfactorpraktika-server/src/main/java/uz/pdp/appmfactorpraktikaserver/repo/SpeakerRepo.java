package uz.pdp.appmfactorpraktikaserver.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.appmfactorpraktikaserver.entity.Speaker;

import java.util.UUID;

public interface SpeakerRepo extends JpaRepository <Speaker, UUID>{



}
