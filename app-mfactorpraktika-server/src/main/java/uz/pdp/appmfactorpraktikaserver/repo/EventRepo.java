package uz.pdp.appmfactorpraktikaserver.repo;


import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.appmfactorpraktikaserver.entity.Event;

import java.util.UUID;

public interface EventRepo extends JpaRepository <Event, UUID>{


}
