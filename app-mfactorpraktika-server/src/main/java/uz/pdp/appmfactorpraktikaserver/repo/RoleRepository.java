package uz.pdp.appmfactorpraktikaserver.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.appmfactorpraktikaserver.entity.Role;
import uz.pdp.appmfactorpraktikaserver.entity.enums.RoleName;

import java.util.List;
import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role,Integer> {

    List<Role> findAllByNameIn(List<RoleName> name);

    //    Optional<Role> findByName(RoleName name);
}
