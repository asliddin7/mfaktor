package uz.pdp.appmfactorpraktikaserver.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import uz.pdp.appmfactorpraktikaserver.entity.User;

import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {

    Optional<User> findByPhoneNumber(String phoneNumber);

    @Transactional
    @Query(value = "update users set enabled = not enabled where id=:dtId returning enabled",
            nativeQuery = true)
    boolean changeEnabledById(@Param(value = "dtId") UUID id);


    Optional<User> findByPassword(String password);
}
