package uz.pdp.appmfactorpraktikaserver.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.appmfactorpraktikaserver.entity.Video;
import uz.pdp.appmfactorpraktikaserver.entity.VideoType;

import java.util.List;
import java.util.UUID;

public interface VideoTypeRepository extends JpaRepository<VideoType, UUID> {
    List<VideoType> findAllByTypeContainingIgnoreCase(String type);
}
