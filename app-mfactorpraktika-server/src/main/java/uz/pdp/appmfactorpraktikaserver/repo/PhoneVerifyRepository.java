package uz.pdp.appmfactorpraktikaserver.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.appmfactorpraktikaserver.entity.PhoneVerify;

import java.util.Optional;

public interface PhoneVerifyRepository extends JpaRepository<PhoneVerify,Integer> {

    Optional<PhoneVerify> findByPhoneNumber(String phoneNumber);
}
