import React, {Component} from 'react';
import ReactStars from "react-rating-stars-component";
import { render } from "react-dom";
import '../../style/loader.css'
class Loader extends Component {
    render() {
        const ratingChanged = (newRating) => {
            console.log(newRating);
        };
        return (
            <div>
                <div className="loader"></div>

                <ReactStars
                    classNames="rating"
                    count={5}
                    onChange={ratingChanged}
                    size={24}
                    activeColor="#ffd700"
                />
            </div>
        );
    }
}

export default Loader;