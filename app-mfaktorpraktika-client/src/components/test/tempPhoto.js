import React, {Component, useState} from 'react';
import axios from 'axios';
import {AvField, AvForm, AvInput} from 'availity-reactstrap-validation';
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"
import Loader from 'react-loader-spinner'

class TempPhoto extends Component {
    constructor(props) {
        super(props);
        this.state = {
            query: "",
            data: [],
            attachment: '',
            loading: false
        }
    }

    componentDidMount() {
        this.query();
    }

    query = () => {
        if (this.state.attachment) {
            axios({
                url: 'http://localhost:8/api/file/' + this.state.data.object.id,
                method: 'get'
            }).then((res) => {
                this.setState({
                    data: res.data.object
                })
                console.log(res.data.object+'OBJEEEECT')
            })
        }
    }


    savePhoto = (e, values) => {
        let formData = new FormData();
        formData.append("request", this.state.attachment)
        axios({
            url: 'http://localhost:8/api/file/saveFile',
            method: 'post',
            data: formData,
            headers: {
                "Content-Type": "multipart/form-data"
            }
        }).then((res) => {
            this.setState({
                loading: true
            })
            console.log(res,"kojoijoijoijoijoij")
            // this.query();
            let tempData=this.state.data
            tempData.push(res.data.object)
            this.setState({data:tempData})
        })
    }

    setPhotoToState = (e) => {
        this.setState({
            attachment: e.target.files[0]
        })
    }

    render() {
        const {attachment, image, loading} = this.state;
        return (
            <div>
                {
                    loading ? <div
                        style={{
                          width: "100%",
                          height: "100",
                          display: "flex",
                          justifyContent: "center",
                          alignItems: "center"
                        }}
                      >
                        <Loader timeout={3000} type="ThreeDots" color="#2BAD60" height="100" width="100" />
                      </div> :

                        <AvForm id={'photoForm'} onSubmit={this.savePhoto}>
                        <input onChange={this.setPhotoToState} type="file" id="image"/>
                        <button type={'submit'} form={'photoForm'}>save</button>
                    </AvForm>
                }

                {this.state.data.map((item, index) =>
                    <div>
                        {item.id?
                            <img src={'http://localhost:8/api/file/' + item.id} alt="#" className='img-fluid'/>
                            :''
                        }
                    </div>)}
            </div>
        );
    }
}
export default TempPhoto;