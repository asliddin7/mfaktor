import React, {Component} from 'react';
import axios from 'axios';
import {Modal, ModalBody, ModalFooter, ModalHeader, Table} from 'reactstrap';
import {AvInput, AvForm} from 'availity-reactstrap-validation';

class TemporarySeat extends Component {
    constructor(props) {
        super(props);
        this.state = {
            query: "",
            data: [],
            filteredData: [],
            modalVisible: false,
            currentItem: '',
            booked:false
        }
    }

    componentDidMount() {
        this.query();
    }

    query = () => {
        axios({
            url: 'http://localhost:8/api/seat/getAllSeats/',
            method: 'get'
        }).then((res) => {
            this.setState({
                data: res.data
            })
        })
    };

    openModal = () => {
        this.setState({
            modalVisible: true
        })
    };
    closeModal = () => {
        this.setState({
            modalVisible: false,
            currentItem: ''
        })
    };

    handleSubmit = (event, errors, values) => {

        if (this.state.currentItem) {
            axios({
                url: 'http://localhost:8/api/seat/editSeat/' + this.state.currentItem.id,
                method: 'put',
                data: values
            }).then((res) => {
                this.closeModal();
                this.query();
            })
        } else {
            axios({
                url: 'http://localhost:8/api/seat/saveSeat/',
                method: 'post',
                data: values
            }).then((res) => {
                this.closeModal();
                this.query();
            })
        }
    };
    render() {
        const {data, modalVisible, currentItem} = this.state;
        return (
            <div>
                <Modal isOpen={modalVisible}>
                    <ModalHeader>
                        <h2>Add seat</h2>
                    </ModalHeader>
                    <ModalBody>
                        <AvForm id={'myForm'} onSubmit={this.handleSubmit}>
                            <AvInput defaultValue={currentItem && currentItem.number} type='text' name={'number'}
                                     placeholder={"Raqami "}/>
                            <AvInput defaultValue={currentItem && currentItem.letter} type='text' name={'letter'}
                                     placeholder={'Harfi (A,B,C,D)'}/>
                            <AvInput defaultValue={currentItem && currentItem.price} type='text' name={'price'}
                                     placeholder={'Narxi'}/>
                            <AvInput defaultValue={currentItem && currentItem.rowNumber} type='text' name={'rowNumber'}
                                     placeholder={'Qator raqami'}/>
                        </AvForm>
                    </ModalBody>
                    <ModalFooter>
                        <button className="btn save" form={'myForm'} type={'submit'}><h3>Save</h3></button>
                        <button className="btn close" onClick={this.closeModal}><h3>Close</h3></button>
                    </ModalFooter>
                </Modal>
                <div className="container-fluid ">
                    <div className="row">
                        <div className="col-md-2 offset-4 my-3">
                            <button onClick={this.openModal}>Open</button>
                        </div>
                    </div>
                    <div className="row">
                        <div className=" offset-2  ">
                            <Table>
                                <div className="row d-flex">
                                    <div className="col-md-8">

                                    {data.map((item, index) => <input type="button"
                                                                      // onClick={this.bookedSeat(item.isBooked)}
                                                                      className={this.state.booked ? "bgGrey seatButton text-center mt-4 ml-4 p-3" : "bgDefault seatButton text-center mt-4 ml-4 p-3"}
                                                                      value={item.rowNumber + item.letter}/> )}
                                </div>
                                </div>
                            </Table>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default TemporarySeat;