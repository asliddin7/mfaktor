import React, {Component} from 'react';
import axios from 'axios'
import {AvForm, AvField} from 'availity-reactstrap-validation';
import Button from "reactstrap/lib/Button";
import {TOKEN} from "../util/utils";
import {Link} from "react-router-dom";
import '../style/registerStyle.css'
import {toast, ToastContainer} from "react-toastify";

class RegistrationPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            registered: false,
            unregistered: false,
            showVerificationInput: false,
            phoneNumber: "",
            all: true,
            redirect: false
        }
    }

    checkPhone = (e) => {
        let event = e.target.value
        if (event.length === 13) {
            axios.get('http://localhost:8/api/auth/getUserByPhone?phone=' + event)
                .then((res) => {
                    if (res.data) {
                        this.setState({
                            registered: true,
                            unregistered: false,
                            showVerificationInput: false
                        })
                    }
                    if (res.data == null) {
                        this.setState({
                            unregistered: true,
                            registered: false,
                            showVerificationInput: false
                        })
                    }
                })
        }
    }
    notifySuccess = () => toast.error("Passwords didn't matches !!!");

    loginOrRegister = (e, v) => {
        // e.preventDefault()
        if (this.state.registered) {
            axios({
                url: 'http://localhost:8/api/auth/login',
                method: 'post',
                data: v
            })
                .then((res) => {
                    this.setState({
                        unregistered: false,
                        registered: false,
                        all: false,
                        showVerificationInput: true,
                        phoneNumber: v.phoneNumber
                    });
                    localStorage.setItem(TOKEN, res.data.body.tokenType + " " + res.data.body.accessToken)
                    axios({
                        url: 'http://localhost:8/api/sms',
                        method: 'post',
                        data: v
                    })
                });

        }
        if (this.state.unregistered) {
            if (v.pd === v.ppd) {
                v = {...v, password: v.pd}
                axios({
                    url: "http://localhost:8/api/auth/saveUser",
                    method: 'post',
                    data: v
                })
                    .then((res) => {
                        this.setState({
                            phoneNumber: v.phoneNumber
                        })
                        localStorage.setItem(TOKEN,res.data.tokenType + " " +res.data.accessToken)
                        axios({
                            url: 'http://localhost:8/api/sms',
                            method: 'post',
                            data: v
                        })
                        this.setState({
                            unregistered: false,
                            registered: false,
                            all: false,
                            showVerificationInput: true
                        });
                    })
            } else {
                this.notifySuccess()
                // alert("Passwords didn't matches !!!")
            }
        }
        if (this.state.showVerificationInput) {
            v = {...v, phoneNumber: this.state.phoneNumber};
            axios({
                url: 'http://localhost:8/api/sms',
                method: 'post',
                data: v
            }).then(res => {
                if (res.data.success) {
                    axios.get('http://localhost:8/api/auth/me', {headers: {"Authorization": localStorage.getItem(TOKEN)}}).then(res => {

                        if (res.data) {

                            if (res.data.roles.filter(item => item.name === 'ROLE_CUSTOMER').length) {
                                this.setState({redirect: true});
                                this.props.history.push("/cabinet/client")
                            }
                            if (res.data.roles.filter(item => item.name === 'ROLE_SUPER_ADMIN').length) {
                                this.setState({redirect: true});
                                this.props.history.push("/cabinet/admin")
                            }
                        }
                    })
                }
            })
        }
    };

    render() {
        return (
            <div className="container-fluid">
                <ToastContainer />
                <div className="row regRow w-100">
                    <div className="col-md-6 imgCol">
                        <img src={require("../images/registerPage.jpg")} alt="" className="registerPage"/>
                    </div>
                    <div className="col-md-6 d-block">
                        <div className="img">
                            <Link to="/mfaktor">
                                <img src={require('../images/originalLogo.png')}
                                     className="logos" alt=""/>
                            </Link>
                        </div>
                        <AvForm
                            className="registerOrLogin"
                            onValidSubmit={this.loginOrRegister}
                        >
                            <>
                                {this.state.all ?
                                    <AvField
                                        onChange={(e) => {
                                            this.checkPhone(e)
                                        }}
                                        name="phoneNumber"
                                        className="inputs"
                                        value="+998"
                                        placeholder="+998_________"
                                        validate={{
                                            // required: {value: true},
                                            pattern: {value: '[+0-9]'},
                                            minLength: {value: 13},
                                            maxLength: {value: 13}
                                        }}
                                    />
                                    : null
                                }

                                {
                                    this.state.registered ?
                                        <AvField placeholder="Parol" name="password" type="password" className="inputs"
                                            validate={{
                                                required: {value: true},
                                                pattern: {value: '^[A-Za-z0-9]+$'},
                                                minLength: {value: 4},
                                                maxLength: {value: 16}
                                            }}
                                        />
                                        : null
                                }
                                {this.state.unregistered ?
                                    <>
                                        <AvField name="firstName" className="inputs"
                                                 placeholder="Ism"/>
                                        <AvField name="lastName" className="inputs"
                                                 placeholder="Familiya"/>
                                        <AvField
                                            required
                                            name="pd" type="password" className="inputs"
                                            placeholder="Parol"/>
                                        <AvField
                                            required
                                            name="ppd" type="password" className="inputs"
                                            placeholder="Takroriy parol"/>

                                    </>
                                    : null
                                }
                                {this.state.all ?
                                    <Button type="submit" className="loginButtonEnter"><b>Kirish</b></Button>
                                    : null}
                            </>

                            {
                                this.state.showVerificationInput ?
                                    <>
                                        <AvField
                                            placeholder="Parolni kiriting:" className="inputs" name="verifyCode"
                                            validate={{
                                                required: {value: true},
                                                pattern: {value: '[0-9]'},
                                                minLength: {value: 6},
                                                maxLength: {value: 6}
                                            }}
                                        />
                                        <button type="submit" className="loginButtonEnter2">Tasdiqlash</button>
                                    </>
                                    : null
                            }
                        </AvForm>
                    </div>
                </div>
            </div>
        );
    }
}

export default RegistrationPage;