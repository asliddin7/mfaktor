import React, {Component} from 'react';
import axios from "axios";
import {PATH} from "../util/utils";
import {Link} from "react-router-dom";
import {BsSearch} from "react-icons/all";
import '../style/headerVideos.css'

class HeaderVideos extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            videoTypes: []
        }
    }

    componentDidMount() {
        this.queryVideoLinks()
        axios.get(PATH + "api/video/getAllVideoTypes").then(res => {
            this.setState({
                videoTypes: res.data
            })
        })
    }

    showInput = () => {
        document.getElementById("video").style.display = "block";
    }

    queryVideoLinks = () => {
        axios({
            url: PATH + 'api/video/getVideo',
            method: 'get'
        }).then(res => {
            this.setState({data: res.data})
        })
    }

    handleSearch=(event)=>{
        axios({
            url:'http://localhost:8/api/video//searchVideoTypes',
            method:'get',
            params:{
                type:event.target.value
            }
        }).then(res=>{
            this.setState({
                data:res.data
            })
        })
    }

    render() {
        const state = this.state

        const getVideosByVideoType = (id) => {
            axios.get(PATH + "api/video/getVideosByVideoType?id=" + id).then(res => {
                this.setState({
                    data: res.data
                })
            })
        }
        return (
            <div className="bgColor">
                <div className="container">

                    <div className="row">
                        <h1 className="vText">Videolar</h1>
                        <div className="col-md-12 d-flex mt-3">


                            {/*<p className="searching">*/}
                            {/*    <BsSearch className="searchIcon" onClick={() => {*/}
                            {/*        this.showInput()*/}
                            {/*    }}/>*/}
                            {/*</p>*/}

                            <input onChange={this.handleSearch} className="videoInput" id={"video"} type="text"
                                   placeholder={"Video nomini kiriting"}/>

                            <div className="videos-btn">
                                <button className="video-btn" onClick={this.queryVideoLinks}>Barchasi</button>
                                {state.videoTypes ? state.videoTypes.map(item =>
                                    <button className="video-btn "
                                            onClick={() => getVideosByVideoType(item.id)}>{item.type}</button>
                                ) : ''}
                            </div>
                        </div>
                    </div>
                    <hr/>

                    <div className="row-body ">
                        <div className="card-group  ml-3 ">

                            {
                                state.data.map((item) => {

                                    let links = "https://youtube.com/embed/";

                                    return (
                                        <div key={item.id} className="cardYu mt-3">
                                            <iframe className="iframe"
                                                    src={links + item.link.substring(item.link.length, item.link.length - 11)}
                                                    width="260"
                                                    height="160"
                                                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"/>
                                        </div>
                                    );
                                })
                            }
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default HeaderVideos;