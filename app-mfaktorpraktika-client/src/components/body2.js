import React, {Component} from 'react';
import {Link} from "react-router-dom";
import '../style/body2.css'
class Body2 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false
        }
    }

    openModal = () => {
        this.setState({
            modalVisible: true
        })
    }
    closeModal = () => {
        this.setState({
            modalVisible: false
        })
    };

    render() {
        const {modalVisible} = this.state
        return (
            <div className="body2">
                <Link to="/cabinet/admin">
                    <div className="btn btn-primary">
                        Admin Panel
                    </div>
                </Link>
                <Link to="/cabinet/client">
                    <div className="btn btn-primary">
                        Cabinet
                    </div>
                </Link>
                <Link to="/temp">
                    <div className="btn btn-primary">
                        Temporary(saveSeat)
                    </div>
                </Link>
                <Link to="/tempPhoto">
                    <div className="btn btn-primary">
                        Temporary(savePhoto)
                    </div>
                </Link>
                <Link to="/loader">
                    <div className="btn btn-primary">
                        Loader
                    </div>
                </Link>

                {/*{this.state.modalVisible ? <div className="youTube">*/}
                {/*    <div className="all">*/}
                {/*        <div className="col">*/}
                {/*            <span className="closeButton" onClick={this.closeModal}>X</span>*/}
                {/*        </div>*/}

                {/*    <iframe className="youTubeVideo" width="850" height="380"*/}
                {/*     src="https://www.youtube.com/embed/6rEnqE2jQoU?start=1"*/}
                {/*            frameBorder="0"*/}
                {/*            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"*/}
                {/*            allowFullScreen></iframe>  </div>*/}
                {/*</div> : ''}*/}

                <h3><span className="m">M</span>Faktor-muvaffaqiyat <br/> va mag'lubiyat omillari</h3>
                <p>O’zbekistonning eng muvaffaqiyatli tadbirkorlaridan <br/> biznes saboqlari
                    va mahorat darslarini olish imkoniyatini <br/> qo’ldan boy bermang.
                    Biznesingizni MFaktor yordamida rivojlantiring.</p>
                <div className="headerVideo">
                    {/*<div className="btn-link" onClick={this.openModal}>*/}
                    {/*    <span className="icon icon-play"><img src={require('../images/play_icon.png')}*/}
                    {/*                                          alt="play"/></span>*/}
                    {/*</div>*/}
                </div>
                <div className="anonslar">
                    <Link to="/headerVideos">
                        <button className="buttonAnonslar button">Anonslar</button>
                    </Link>
                    <Link to="/enterRegistration">
                        <button className="buttonKirish button">Kirish</button>
                    </Link>
                </div>
                <div className="tadbirlar ">
                    <h1>Navbatdagi tadbirlar</h1>
                    <Link to="/headerEvents" className="buttonBarchasi">
                        <button className="button">Barchasi</button>
                    </Link>
                </div>
                <h2>Hozirda kutilayotgan tadbirlar mavjud emas</h2>
                <div className="tadbirlar">
                    <h1>Tadbirlar videolari</h1>
                    <Link to="/headerVideos" className="buttonBarchasi">
                        <button className="button">Barchasi</button>
                    </Link>
                </div>
            </div>
        );
    }
}
export default Body2;