import React, {Component} from 'react';
import '../style/speakers.css'
import {Link} from "react-router-dom";

class Speakers extends Component {
    render() {
        return (
            <div>


                <div className="spikerlar ">
                    <div className="spikerlar1">
                        <h2>MFaktor spikerlari</h2>
                        <p>Tadbirlarni olib boruvchi mutaxasislar O’zbekistonning <br/>
                            muvaffaqiyatli tadbirkorlari, xalqaro darajadagi huquqshunoslar <br/>
                            va o’z kasbining ustalari olib borishadi.</p>
                    </div>
                    <div className="spikerlar2">
                        <Link to="/headerSpeakers">
                            <button className="buttonBarchasi button">Barchasi</button>
                        </Link>

                    </div>
                </div>

                <div className="allSpeakers ">
                    <div className="brand">
                        <img src={require('../images/logo.jpg')} alt=""/>
                    </div>
                    <div className="speakers">
                        <img src={require('../images/nodiraka.jpg')} alt="" className="img1"/>
                        <img src={require('../images/bobiraka.jpg')} alt="" className="img2"/>
                        <img src={require('../images/lazizaka.jpg')} alt="" className="img3"/>
                        <img src={require('../images/hasanaka.jpg')} alt="" className="img4"/>
                        <img src={require('../images/husanaka.jpg')} alt="" className="img5"/>
                        <img src={require('../images/umidaka.jpg')} alt="" className="img6"/>
                        <img src={require('../images/murodaka.jpg')} alt="" className="img7"/>
                        <img src={require('../images/zafaraka.jpg')} alt="" className="img8"/>
                        <img src={require('../images/akmalaka.jpg')} alt="" className="img9"/>
                        <img src={require('../images/alisheraka.jpg')} alt="" className="img10"/>
                        <img src={require('../images/hikmataka.jpg')} alt="" className="img11"/>
                    </div>
                </div>
                <div className="tadbirlar ">
                    <h1>MFaktor haqida <br/>boshqalar fikri</h1>
                </div>
                <h2 className="text-center pb-5">Hozirda sharhlar mavjud emas</h2>
            </div>
        );
    }
}

export default Speakers;