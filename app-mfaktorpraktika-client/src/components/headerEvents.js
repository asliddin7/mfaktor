import React, {Component} from 'react';
import {BsSearch} from "react-icons/bs";
import {Link} from "react-router-dom";
import {BiRightArrowAlt} from "react-icons/all";
class HeaderEvents extends Component {
    render() {
        return (
            <div className="bgColor">
                <div className="waiting">
                    <h2 className="pt-5 mb-5 text-center ">Kutilayotgan tadbirlar</h2>
                </div>

                <div className="container mt-5">
                    <div className="row bgColor">
                        <div className="col-md-4 bg-white p-0 mt-3 mb-5">
                            <div className="event-card mainCard">
                                <img src={require('../images/17 10 20.jpg')} alt=""
                                     className="w-100 img-fluid rounded-top"/>

                                <div className="bodyCard pt-4">
                                    <h4 className="text-center"><b><i>Sotuv konveyeri</i></b></h4>
                                    <div className="row ml-4 mt-5">
                                        <div className="col-md-6  text-muted"><p>Spiker</p></div>
                                        <div className="col-md-6  text-muted"><p>Vaqti</p></div>
                                    </div>
                                    <div className="row ml-4">
                                        <div className="col-md-6 "><h4>Hasan Mamasaidov</h4></div>
                                        <div className="col-md-6 "><h4>10-10-2020 20:00</h4></div>
                                    </div>
                                    <div className="card-footer cardFooter bg-light mt-5">
                                        <div className="row bg-light ">
                                            <div className=" col-md-6 prices text-center ">
                                                <span className="price mr-2"><b>800000</b></span>
                                                <span>UZS</span>
                                            </div>
                                            <div className="col-md-6 text-center">
                                                <span className="price mr-2"><b>1000000</b></span>
                                                <span>UZS</span>
                                            </div>

                                        </div>
                                        <div className="row">
                                            <div className="col-md-12 text-center mt-3 text-muted">
                                                <p><i>Tadbir narxi shu oraliqda</i></p>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="footer">
                    <h1><span className="m">M</span>Faktor</h1>
                </div>
            </div>

        );
    }
}
export default HeaderEvents;