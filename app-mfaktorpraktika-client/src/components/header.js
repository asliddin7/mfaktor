import React, {Component} from 'react';
import { Link} from 'react-router-dom'
import "./registrationPage";

class Header extends Component {
    constructor(props) {
        super(props);
        this.state={
            data: [],
            filteredData: [],
            modalVisible: false,
            currentItem: ''
        }
    }

    openModal = () => {
        this.setState({
            modalVisible: true
        })
    };
    render() {

        return (
            <div>
                <div className="header bg-white">
                    <Link to={"/mfaktor"}>
                    <a href="#" className="logo"><img src={require('../images/logo.jpg')} /></a>
                        </Link>
                    <ul>
                        <li><Link to={"/headerEvents"}>
                            <a href="#">Tadbirlar</a>
                        </Link></li>

                        <li><Link to={"/headerSpeakers"}>
                            <a href="#">Spikerlar</a>
                        </Link></li>

                        <li><Link to={"/headerVideos"}>
                            <a href="#">Videolar</a>
                        </Link></li>



                    </ul>

                        <ul className="ul2">
                            <li><h2><span className="cod">(99) </span>802-00-88</h2></li>
                            <li className="enterRegistration">
                                <Link to="/enterRegistration">
                                    <button className="buttonK button">Kirish</button>
                                </Link>
                            </li>
                        </ul>


                </div>

            </div>
        );
    }
}

export default Header;