import React, {Component} from 'react';

class Videos extends Component {
    render() {
        return (
            <div>
                <div className="videolar">
                    <div className="left ">
                        <iframe className="img" src="https://www.youtube.com/embed/U9q_aJ74thc" frameBorder="0"
                                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                allowFullScreen></iframe>
                        <div className="text">
                            <p>MFaktor - 65 | Murod Nazarov | "Murad Buildings" asoschisi</p>
                        </div>
                    </div>

                    <div className="right ">
                        <div className="right1">
                            <div className="img1 ">
                                <iframe className="img" src="https://www.youtube.com/embed/7XhuuqMPsYo"
                                        frameBorder="0"
                                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                        allowFullScreen></iframe>

                                <div className="text1">
                                    <p>MFaktor - 65 | Shuhrat Holtayev  "Kuchli servisning afzalligi"</p>
                                </div>
                            </div>
                            <div className="img1 ">
                                <iframe className="img" src="https://www.youtube.com/embed/Gyl9aatMeu8?start=5"
                                        frameBorder="0"
                                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                        allowFullScreen></iframe>

                                <div className="text1">
                                    <p>MFaktor - 65 | Zafar Hoshimov "Tadbirkorlik o'xshamasa.."</p>
                                </div>
                            </div>
                        </div>

                        <div className="right2">
                            <div className="img1">
                                <iframe className="img" src="https://www.youtube.com/embed/6eRKPrUWkKE"
                                        frameBorder="0"
                                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                        allowFullScreen></iframe>

                                <div className="text1">
                                    <p>MFaktor - 65 | Voronka Uz "Siz 100$lik pul emassiz.."</p>
                                </div>
                            </div>
                            <div className="img1">
                                <iframe className="img" src="https://www.youtube.com/embed/6rEnqE2jQoU" frameBorder="0"
                                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                        allowFullScreen></iframe>

                                <div className="text1">
                                    <p>MFaktor - 65 | Murod Nazarov | "Murad Buildings" asoschisi</p>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        );
    }
}

export default Videos;