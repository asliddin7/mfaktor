import React, {Component} from 'react';
import {FiFacebook, FiPhone} from "react-icons/fi";
import {RiYoutubeLine} from "react-icons/ri";
import {FaTelegramPlane} from "react-icons/fa";
import {SiInstagram} from "react-icons/si";
import {Link} from "react-router-dom";

class Footer extends Component {
    render() {
        return (
            <div>
                <div className="footer">
                    <a href="#" className="logo"><img src={require('../images/logo.jpg')} /></a>
                    <ul className="ul1">

                        <li><Link to={"/headerEvents"}>
                            <a href="#">Tadbirlar</a>
                        </Link></li>

                        <li><Link to={"/headerSpeakers"}>
                            <a href="#">Spikerlar</a>
                        </Link></li>

                        <li><Link to={"/headerVideos"}>
                            <a href="#">Videolar</a>
                        </Link></li>

                    </ul>
                    <div className="footerNumber ml-auto">
                        <ul className="ul3">
                            <li><h2><FiPhone className="mr-2"/><span className="cod">(99) </span>802-00-88</h2></li>
                        </ul>
                        <ul className=" icons pt-0 ">
                            <li><a href="https://www.youtube.com/"><RiYoutubeLine /></a></li>
                            <li><a href="https://t.me/MFaktorUz"><FaTelegramPlane/></a></li>
                            <li><a href="https://www.facebook.com/MFaktorUz/"><FiFacebook/></a></li>
                            <li><a href="https://www.instagram.com/mfaktoruz/"><SiInstagram/></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}

export default Footer;