import React, {Component} from 'react';
import '../../style/settings.css'
import CabinetPageHeader from "./cabinetPageHeader";
import {IoIosCloseCircleOutline} from "react-icons/io";
import {Link} from "react-router-dom";
import {ToastContainer, toast} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import axios from "axios";
import {TOKEN} from "../../util/utils";


class ChangePassword extends Component {
    constructor(props) {
        super(props);
        this.state={
            currentUser: '',
            data: [],
            password:''
        }
    }

    notifyError = () => toast.error("Xatolik !!! ");

    componentDidMount() {
        // this.getUser();
    }
    render() {
        return (
            <div className="bgColor">
                <CabinetPageHeader/>
                <div className="container">
                    <ToastContainer/>
                    <h2 className="mt-5 user">Foydalanuvchi ma’lumotlari</h2>
                    <div className="modalSettings w-75 ">
                        <Link to={"/cabinet/client"}>
                            <span className="exitSpan float-right"><IoIosCloseCircleOutline/></span>
                        </Link>
                        <div className="row bg-white">
                            <div className="col-md-6 text-center mt-5 mb-3 offset-3">
                                <div className="form mb-3">
                                    <input className="pt-3 pl-3" type="password" name="name" autoComplete="off"
                                           required/>
                                    <label className="label-name">
                                        <span className="content-name">Eski parolingiz</span>
                                    </label>
                                </div>
                                <div className="form mb-3">
                                    <input className="pt-3 pl-3" type="password" name="name" autoComplete="off"
                                           required/>
                                    <label className="label-name">
                                        <span className="content-name">Yangi parolingiz</span>
                                    </label>
                                </div>
                                <div className="form mb-3">
                                    <input className="pt-3 pl-3" type="password" name="name" autoComplete="off"
                                           required/>
                                    <label className="label-name">
                                        <span className="content-name">Yangi parol takroran</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div className="row bg-white">
                            <div className="offset-7 mb-5 mt-3">
                                <button type={"submit"} className="btn save ml-3 " onClick={this.notifyError}>Saqlash</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="footer">
                    <h1>MFaktor</h1>
                </div>
            </div>
        );
    }
}

export default ChangePassword;