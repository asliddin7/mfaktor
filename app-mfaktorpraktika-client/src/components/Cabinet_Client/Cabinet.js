import React, {Component} from 'react';
import '../../style/Cabinet.css'
import {Link} from "react-router-dom";
import {TiPlus} from "react-icons/ti"
import CabinetPageHeader from "./cabinetPageHeader";
import {BiRightArrowAlt} from "react-icons/all";

class Cabinet extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            filteredData:[],
            modalVisible:false,
            currentItem:''
        }
    }

    openModal=()=>{
        this.setState({
            modalVisible:true
        })
    }
    closeModal=()=>{
        this.setState({
            modalVisible:false,
            currentItem:''
        })
    }

    handleSubmit=()=>{

    }

    render() {

        return (
            <div className="bgColor">
                <CabinetPageHeader/>
                <div className="container">
                    <h2 className="mt-5">Mening tadbirlarim</h2>
                    <h4 className="text-center">Hozirda tadbirlar mavjud emas</h4>
                    <Link to={"/headerEvents"}>
                        <div className="card longCard">
                            <div className="card-body longCardBody d-flex justify-content-center">
                                <span><TiPlus/></span>
                                <p className="ml-2">Tadbirga ro'yhatdan o'tish</p>
                            </div>
                        </div>
                    </Link>
                    <h2 className="mt-5">Kutilayotgan tadbirlar</h2>
                    <div className="row bgColor">
                        <div className="col-md-4 bg-white p-0 mt-3 mb-5">
                            <div className="event-card mainCard">
                                <img src={require('../../images/17 10 20.jpg')} alt=""
                                     className="w-100 img-fluid rounded-top"/>

                                <div className="bodyCard pt-4">
                                    <h4 className="text-center"><b><i>Sotuv konveyeri</i></b></h4>
                                    <div className="row ml-4 mt-5">
                                        <div className="col-md-6  text-muted"><p>Spiker</p></div>
                                        <div className="col-md-6  text-muted"><p>Vaqti</p></div>
                                    </div>
                                    <div className="row ml-4">
                                        <div className="col-md-6 "><h4>Hasan Mamasaidov</h4></div>
                                        <div className="col-md-6 "><h4>10-10-2020 20:00</h4></div>
                                    </div>
                                    <div className="card-footer cardFooter bg-light mt-5">
                                        <div className="row bg-light ">
                                            <div className=" col-md-6 prices text-center ">
                                                <span className="price mr-2"><b>800000</b></span>
                                                <span>UZS</span>
                                            </div>
                                            <div className="col-md-6 text-center">
                                                <span className="price mr-2"><b>1000000</b></span>
                                                <span>UZS</span>
                                            </div>

                                        </div>
                                        <div className="row">
                                            <div className="col-md-12 text-center mt-3 text-muted">
                                                <p><i>Tadbir narxi shu oraliqda</i></p>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <Link to={"/regEvent"}>
                                    <div className="row booking d-flex">
                                        <div className="col-md-9 mt-3">
                                            <p>Tadbir uchun joy <br/> band qilish</p>
                                        </div>
                                        <div className="col-md-3 mt-3">
                                            <span className="spanRight"><BiRightArrowAlt/></span>
                                        </div>
                                    </div>
                                </Link>

                            </div>
                        </div>
                    </div>


                </div>
                <div className="footer">
                    <h1><span className="m">M</span>Faktor</h1>
                </div>
            </div>
        );
    }
}

export default Cabinet;