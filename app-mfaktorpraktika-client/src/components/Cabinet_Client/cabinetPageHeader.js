import React, {Component} from 'react';
import {Link} from "react-router-dom";
import '../../style/Cabinet.css'
import {FiHome} from "react-icons/fi"
import {FiSettings} from "react-icons/fi"
import {FiKey} from "react-icons/fi"
import {IoMdExit} from "react-icons/io"
import {Popup} from "reactjs-popup";
import axios from "axios";
import {TOKEN} from "../../util/utils";
class CabinetPageHeader extends Component {

    constructor(props) {
        super(props);
        this.state={
            currentUser:''
        }
    }
    componentDidMount() {
        axios.get('http://localhost:8/api/auth/me', {headers: {"Authorization": localStorage.getItem(TOKEN)}}).
        then(res => {
            this.setState(
                {
                    currentUser:res.data
                }
            )
        })
    }

    render() {

        return (

            <div className="header bg-white">
                <Link to={"/mfaktor"}>
                    <a href="#" className="logo"><img className="float-left" src={require('../../images/logo.jpg')}/></a>
                </Link>
                <ul className="mt-1 ml-0">
                    <li><Link to={"/cabinet/client"}>
                        <a href="#">Mening tadbirlarim</a>
                    </Link></li>

                    <li><Link to={"/headerEvents"}>
                        <a href="#">Tadbirlar</a>
                    </Link></li>
                </ul>

                <ul className="ul2 d-flex float-right">
                    <li><img className="man" src={require('../../images/man.png')} alt=""/></li>
                    <Popup
                        trigger={<div className="name"> {this.state.currentUser.firstName} <br/>{this.state.currentUser.lastName} </div>}
                        on="click"

                    >
                        <div className="dropdownCabinet">
                            <div className="balance">
                                <h5>Sizning balansingiz:</h5>
                                <div className="d-flex">
                                    <h1>0</h1>
                                    <p className="mt-3 ml-3">UZS</p>
                                </div>

                            </div>
                            <ul className="">
                                <Link to={"/cabinet/client"} className="cabLi d-flex"><span className="homeCab ml-3 mt-2"><FiHome/></span><h6
                                    className="mt-4 ml-3">Kabinet</h6></Link>
                                <Link to={"/settings"} className="cabLi d-flex"><span
                                    className="homeCab ml-3 mt-2"><FiSettings/></span><h6 className="mt-4 ml-3">Sozlamalar</h6>
                                </Link>
                                <Link to={"/changePassword"} className="cabLi d-flex"><span
                                    className="homeCab ml-3 mt-2"><FiKey/></span><h6 className="mt-4 ml-3">Parol
                                    o'zgartirish</h6></Link>
                                <Link to={"/mfaktor"} className="cabLi d-flex"><span className="homeCab ml-3 mt-2"><IoMdExit/></span>
                                    <h6 className="mt-4 ml-3">Chiqish</h6></Link>
                            </ul>
                        </div>
                    </Popup>
                </ul>
            </div>

        );
    }
}

export default CabinetPageHeader;