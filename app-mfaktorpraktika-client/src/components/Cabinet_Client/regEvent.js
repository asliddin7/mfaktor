import React, {Component, useRef} from 'react';
import '../../style/regEvent.css'
import CabinetPageHeader from "./cabinetPageHeader";
import {AvForm, AvField, AvInput, AvGroup, AvFeedback} from 'availity-reactstrap-validation';
import axios from "axios";
import {TOKEN} from "../../util/utils";
import {AiOutlineCheck, GrFormNext, GrFormPrevious} from "react-icons/all";
import {ToastContainer, toast} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Button from "react-bootstrap/Button";

class RegEvent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentUser: '',
            currentPage: 1,
            query: "",
            data: [],
            filteredData: [],
            modalVisible: false,
            currentItem: '',
            bookedSeats: [],
            booked: false,
            id:'',
            birthDate:'',
            companyName:'',
            profession:'',
            region:''
        }
    }

    page2 = () => {
        this.setState({
            currentPage: 2
        })
    }
    page3 = () => {
        this.setState({
            currentPage: 3
        })
    }
    prevPage1 = () => {
        this.setState({
            currentPage: 1
        })
    }
    prevPage2 = () => {
        this.setState({
            currentPage: 2
        })
    }

    notifySuccess = () => toast.success("Siz joy band qildingiz!");

    componentDidMount() {
        // this.notifySuccess();
        this.query();
        axios.get('http://localhost:8/api/auth/me', {headers: {"Authorization": localStorage.getItem(TOKEN)}}).then(res => {
            this.setState(
                {
                    currentUser: res.data,
                    id: res.data.id
                }
            )
        })
    }

    query = () => {
        axios({
            url: 'http://localhost:8/api/seat/getAllSeats/',
            method: 'get'
        }).then((res) => {
            this.setState({
                data: res.data
            })
        })
    };

    saveUser = (e, err, v) => {
        console.log(v.region + 'VVVVVV')
        v = {...v, id: this.state.id}
        axios({
            url: 'http://localhost:8/api/auth/saveUser',
            method: 'post',
            data: v
        }).then((res) => {
            console.log("wwwwwwwwwwwwwww" + res.data)
        })
    }

    saveBookedSeat = () => {
        axios({
            url: 'http://localhost:8/api/seat/bookedSeat?seatIds='+this.state.bookedSeats,
            method: 'get'
        }).then((res) => {
            if (res.data.success){
                this.notifySuccess();
            }
            console.log(res,"RESPONSE")
        })
    }

    birthDate = (v) =>{
        this.setState({
            birthDate:v
        })
    }
    companyName = (v) =>{
        this.setState({
            companyName:v
        })
    }
    profession = (v) =>{
        this.setState({
            profession:v
        })
    }
    region = (v) =>{
        this.setState({
            region:v
        })
    }

    render() {
        const {data, modalVisible, currentItem,bookedSeats} = this.state;
        const {currentUser, currentPage} = this.state;
        const  bookedSeat = (bandQilingan,id,index) => {
            let tempArray=data;
            tempArray[index].booked=!bandQilingan
            this.setState({data:[...tempArray]})
            if (!bandQilingan){
                let tempBookedArray=bookedSeats;
                tempBookedArray.push(id)
                this.setState({bookedSeats:tempBookedArray})
            }else{
                let tempBookedArray=bookedSeats;
                for (let i = 0; i < tempBookedArray.length; i++) {
                    if (tempBookedArray[i]===id){
                        tempBookedArray.splice(i,1)
                    }
                }
                this.setState({bookedSeats:tempBookedArray})
            }
            console.log(data,"DATA")
            console.log(bookedSeats,"DATA===2")
        }

        return (
            <div className="bgColor">
                <CabinetPageHeader/>
                <div className="container">
                        <ToastContainer />
                    <div className="row">
                        <div className="col-md-12">
                            <div className="card-list cardList">
                                <div className="row">
                                    <div className="col-md-4">
                                        <img src={require('../../images/17 10 20.jpg')} alt="#" className="imgReg"/>
                                    </div>
                                    <div className="col-md-4">
                                        <h4 className="mt-4 text-center">Vafodorlik tizimi orqali savdoni oshirish</h4>
                                        <div className="row ml-4 mt-4">
                                            <div className="col-md-6  text-muted"><p>Spiker</p></div>
                                            <div className="col-md-6  text-muted"><p>Vaqti</p></div>
                                        </div>
                                        <div className="row ml-4">
                                            <div className="col-md-6 "><h4><b>Hasan Mamasaidov</b></h4></div>
                                            <div className="col-md-6 "><h4><b>10-10-2020 20:00</b></h4></div>
                                        </div>
                                    </div>
                                    <div className="col-md-4 mt-4">
                                        <div className="d-flex">
                                            <h1>0</h1>
                                            <button className="btn btn-outline-danger eventButton ml-4">Tadbir haqida
                                            </button>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-12">
                            <div className="card-list bg-white">
                                <div className="row">
                                    <div className="col-md-4">
                                        <h4 className="offset-2 mt-5">Tadbirga ro'yhatdan o'tish</h4>
                                    </div>
                                </div>

                                {currentPage === 1 ?

                                    <div>
                                        <div className="row mt-5">
                                            <div className="col-md-2 " style={{marginLeft:510}}>
                                                <h1 className="regNumber">1</h1>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-md-4">
                                                <h4 className="offset-2 mt-5">Asosiy ma'lumotlar</h4>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <AvForm className="avForm d-flex">
                                                <AvField value={currentUser.firstName} name="firstName"
                                                         className="avField " placeholder="Ism"/>
                                                <AvField value={currentUser.lastName} name="lastName"
                                                         className="avField offset-1" placeholder="Familiya"/>
                                                <AvField value={currentUser.phoneNumber} name="phoneNumber"
                                                         className="avField offset-2"/>
                                                <AvField name="birthDate" className="avField ml-5" type="date" onchange={this.birthDate}/>
                                            </AvForm>
                                        </div>
                                        <div className="row mb-5">
                                            <div className="col-md-12">
                                                <div className="col-md-3 float-right">
                                                    <button onClick={this.page2}
                                                            className="btn nextButton mb-5">Keyingisi <GrFormNext
                                                        style={{marginBottom: 1}}/>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    : null
                                }
                                {
                                    currentPage === 2 ?
                                        <div>
                                            <div className="row mt-5">
                                                <div className="col-md-2 " style={{marginLeft:510}}>
                                                    <h1 className="regNumber">2</h1>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-md-4">
                                                    <h4 className="offset-2 mt-5">Qo'shimcha ma'lumotlar</h4>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <AvForm className="avForm d-flex ">
                                                    <AvField name="companyName" className="avField2"
                                                             placeholder="Tashkilot nomi" onchange={this.companyName}/>
                                                    <AvField type="select" className="avField2 ml-5" name="select" onchange={this.profession}>
                                                        <option>Lavozimi</option>
                                                        <option>Biznes egasi</option>
                                                        <option>Kompaniya rahbari</option>
                                                        <option>Ish boshqaruvchi</option>
                                                        <option>Tashkilot hodimi</option>
                                                        <option>Talaba</option>
                                                    </AvField>
                                                    <AvField type="select" className="avField2 ml-5" name="select" onchange={this.region}>
                                                        <option>Viloyati</option>
                                                        <option>Toshkent shahri</option>
                                                        <option>Toshkent viloyati</option>
                                                        <option>Farg'ona viloyati</option>
                                                        <option>Andijon viloyati</option>
                                                        <option>Namangan viloyati</option>
                                                        <option>Sirdaryo viloyati</option>
                                                        <option>Samarqand viloyati</option>
                                                        <option>Navoiy viloyati</option>
                                                        <option>Buxoro viloyati</option>
                                                        <option>Jizzax viloyati</option>
                                                        <option>Termiz viloyati</option>
                                                        <option>Qashqadaryo viloyati</option>
                                                        <option>Surxondaryo viloyati</option>
                                                    </AvField>

                                                </AvForm>
                                            </div>
                                            <div className="row mb-5">
                                                <div className="col-md-12 d-flex">
                                                    <div className="col-md-3 offset-6">
                                                        <button onClick={this.prevPage1} className="btn prevButton mb-5"
                                                                style={{marginLeft: 100}}><GrFormPrevious
                                                            style={{marginBottom: 1}}/>Orqaga
                                                        </button>
                                                    </div>
                                                    <div className="col-md-3">
                                                        <button onClick={this.page3}
                                                                className="btn nextButton mb-5">Keyingisi <GrFormNext
                                                            style={{marginBottom: 1}}/>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        : null}

                                {currentPage === 3 ?
                                    <div>
                                        <div className="row mt-5">
                                            <div className="col-md-2 " style={{marginLeft:510}}>
                                                <h1 className="regNumber">3</h1>
                                            </div>
                                        </div>
                                        <div className="row mt-5 mb-3">
                                            <h5 className="ml-5">Joylar</h5>
                                            <ul className="d-flex ml-auto mr-5">
                                                <li style={{listStyle: "none", marginRight: 15}}
                                                    className="d-flex align-items-center mr-5"><span
                                                    className="placeLi"></span>Sotilgan joylar
                                                </li>
                                                <li style={{listStyle: "none", marginRight: 15}}
                                                    className="d-flex align-items-center mr-5"><span
                                                    className="placeLi placeLi2"></span>Band joylar
                                                </li>
                                                <li style={{listStyle: "none"}} className="d-flex align-items-center">
                                                    <span className="placeLi placeLi3"></span>Bo'sh joylar
                                                </li>
                                            </ul>
                                        </div>
                                        <AvForm id={'seatForm'} onSubmit={this.saveBookedSeat}>
                                            <div className="row d-flex">
                                                <div className="col-md-5">
                                                    {console.log(data,"ARRAY")}
                                                    {data?data.map((item, index) => <input type="button"
                                                                                      onClick={()=>bookedSeat(item.booked,item.id,index)}
                                                                                      className={item.booked ? "bgRed  seatButton text-center mt-4 ml-4 p-3" : "bgDefault seatButton text-center mt-4 ml-4 p-3"}
                                                                                      value={item.rowNumber + item.letter}/>):''}
                                                </div>
                                            </div>

                                            <div className="row mb-5">
                                                <div className="col-md-12 d-flex">
                                                    <div className="col-md-3 offset-6">
                                                        <button onClick={this.prevPage2} className="btn prevButton mb-5"
                                                                style={{marginLeft: 100}}><GrFormPrevious
                                                            style={{marginBottom: 1}}/>Orqaga
                                                        </button>
                                                    </div>
                                                    <div className="col-md-3">
                                                        <button className="btn nextButton mb-5" type={'submit'} form={'seatForm'}>Saqlash <AiOutlineCheck
                                                            style={{marginBottom: 1}}/></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </AvForm>

                                    </div>

                                    : null}

                            </div>
                        </div>
                    </div>
                </div>
                <div className="footer">
                    <h1><span className="m">M</span>Faktor</h1>
                </div>
            </div>
        );
    }
}

export default RegEvent;