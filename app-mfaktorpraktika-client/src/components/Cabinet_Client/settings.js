import React, {Component} from 'react';
import '../../style/settings.css'
import axios from 'axios';
import {IoIosCloseCircleOutline} from "react-icons/io";
import {Link} from "react-router-dom";
import CabinetPageHeader from "./cabinetPageHeader";
import {FiUser} from "react-icons/fi";
import 'react-day-picker/lib/style.css';
import {TOKEN} from "../../util/utils";
import {AvForm, AvField, AvInput} from 'availity-reactstrap-validation';
import {ToastContainer, toast} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

class Settings extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentUser: '',
            data: [],
            id: '',
            firstName:'',
            lastName:'',
            phoneNumber:'',
            password:''
        }
    }

    componentDidMount() {
        this.getUser();
    }

    getUser = () => {
        axios.get('http://localhost:8/api/auth/me', {headers: {"Authorization": localStorage.getItem(TOKEN)}}).then(res => {
            this.setState(
                {
                    currentUser: res.data,
                    id: res.data.id,
                    firstName:res.data.firstName,
                    lastName:res.data.lastName,
                    phoneNumber:res.data.phoneNumber,
                    password:res.data.password
                }
            )
        })
    }

    notifySuccess = () => toast.success("Foydalanuvchi ma'lumotlari o'zgartirildi ");

    editUser = (e, err, v) => {
        console.log(v.region + 'VVVVVV')
        v = {...v,
            id: this.state.id,
            firstName:this.state.firstName,
            lastName: this.state.lastName,
            phoneNumber: this.state.phoneNumber,
            password: this.state.password
        }
        axios({
            url: 'http://localhost:8/api/auth/editUser',
            method: 'put',
            data: v
        }).then((res) => {
            if (res.data.success){
                this.notifySuccess();
            }
        })
    }

    render() {
        const {currentUser} = this.state;
        return (
            <div className="bgColor">
                <CabinetPageHeader/>
                <div className="container">
                    <ToastContainer />
                    <h2 className="mt-2 user">Foydalanuvchi ma’lumotlari</h2>
                    <AvForm>
                        <div className="modalSettings bg-white ml-3">
                            <Link to={"/cabinet/client"}>
                                <span className="exitSpan float-right"><IoIosCloseCircleOutline/></span>
                            </Link>
                            <div className="row bg-white">
                                <div className="col-md-12 manImg mt-4 text-center ml-3">
                                    <span className="userIcon"><FiUser/></span>
                                </div>
                            </div>
                            <AvForm id={'userFrom'} onSubmit={this.editUser}>
                            <div className="row bg-white">
                                <div className="col-md-8 text-center mt-3 mb-3 offset-2">
                                    <div className="form">
                                        <AvInput type="text" name="companyName" autoComplete="off" required/>
                                        <label className="label-name">
                                            <span className="content-name">Tashkilot nomi</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                                <div className="row   bg-white">
                                    <div className="col-md-4 mb-3 offset-2">
                                        <div className="form mb-3">
                                            <input defaultValue={currentUser.firstName} type="text" name="firstName"
                                                   autoComplete="off" required/>
                                            <label className="label-name">
                                                <span className="content-name">Ism</span>
                                            </label>
                                        </div>
                                        <div className="form mb-3">
                                            <input defaultValue={currentUser.lastName} type="text" name="lastName"
                                                   autoComplete="off" required/>
                                            <label className="label-name">
                                                <span className="content-name">Familiyasi</span>
                                            </label>
                                        </div>
                                        <div className="form">
                                            <AvField type="select" name="region" autoComplete="off" className="input form-control">
                                                <option>Viloyati</option>
                                                <option value="Toshkent shahri">Toshkent shahri</option>
                                                <option value="Toshkent viloyati">Toshkent viloyati</option>
                                                <option value="Farg'ona viloyati">Farg'ona viloyati</option>
                                                <option value="Andijon viloyati">Andijon viloyati</option>
                                                <option value="Namangan viloyati">Namangan viloyati</option>
                                                <option value="Sirdaryo viloyati">Sirdaryo viloyati</option>
                                                <option value="Sirdaryo viloyati">Sirdaryo viloyati</option>
                                                <option value="Navoiy viloyati">Navoiy viloyati</option>
                                                <option value="Buxoro viloyati">Buxoro viloyati</option>
                                                <option value="Jizzax viloyati">Jizzax viloyati</option>
                                                <option value="Termiz viloyati">Termiz viloyati</option>
                                                <option value="Qashqadaryo viloyati">Qashqadaryo viloyati</option>
                                                <option value="Surxondaryo viloyati">Surxondaryo viloyati</option>
                                            </AvField>
                                            {/*<label className="label-name">*/}
                                            {/*    <span className="content-name">Viloyati</span>*/}
                                            {/*</label>*/}
                                        </div>
                                    </div>
                                    <div className="col-md-4">
                                        <div className="form mb-3">
                                            <AvField type="date" name="birthDate" autoComplete="off" required/>
                                        </div>
                                        <div className="form mb-3">
                                            <input defaultValue={currentUser.phoneNumber} type="text" name="phoneNumber"
                                                   autoComplete="off" required/>
                                            <label className="label-name">
                                                <span className="content-name">Telefon raqami</span>
                                            </label>
                                        </div>
                                        <div className="form">
                                            <AvField type="select" name="profession" autoComplete="off" className="input form-control">
                                                <option>Lavozimi</option>
                                                <option value="Biznes egasi">Biznes egasi</option>
                                                <option value="Kompaniya rahbari">Kompaniya rahbari</option>
                                                <option value="Ish boshqaruvchi">Ish boshqaruvchi</option>
                                                <option value="Tashkilot hodimi">Tashkilot hodimi</option>
                                                <option value="Talaba">Talaba</option>
                                            </AvField>
                                            {/*<label className="label-name">*/}
                                            {/*    <span className="content-name">Lavozimi</span>*/}
                                            {/*</label>*/}
                                        </div>
                                    </div>
                                    <div className="offset-7 mb-5">
                                        <button type="submit" form='userFrom' className="btn save ml-3">Saqlash</button>
                                    </div>
                                </div>
                            </AvForm>
                        </div>
                    </AvForm>
                </div>
                <div className="footer">
                    <h1>MFaktor</h1>
                </div>
            </div>
        );
    }
}

export default Settings;