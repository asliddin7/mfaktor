import React, {Component} from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom'
import MainPage from "./mainPage";
import HeaderEvents from "./components/headerEvents";
import HeaderSpeakers from "./components/headerSpeakers";
import HeaderVideos from "./components/headerVideos";
import Settings from "./components/Cabinet_Client/settings";
import ChangePassword from "./components/Cabinet_Client/changePassword";
import regEvent from "./components/Cabinet_Client/regEvent";
import RegistrationPage from "./components/registrationPage";
import TemporarySeat from "./components/test/temporary(Seat)";
import TempPhoto from "./components/test/tempPhoto";
import Cabinet from "./components/Cabinet_Client/Cabinet";
import Loader from "./components/test/loader";
import AdminPage from "./adminPanel/AdminPage";

class App extends Component {

    render() {
        return (
            <div>

                <BrowserRouter>
                    <Switch>
                        <Route  exact path={'/'} component={MainPage}/>
                        <Route exact path={'/mfaktor'} component={MainPage}/>
                        <Route exact path={'/headerEvents'} component={HeaderEvents}/>
                        <Route exact path={'/headerSpeakers'} component={HeaderSpeakers}/>
                        <Route exact path={'/headerVideos'} component={HeaderVideos}/>
                        <Route exact path={'/enterRegistration'} component={RegistrationPage}/>
                        <Route exact path={'/cabinet/client'} component={Cabinet}/>
                        <Route exact path={'/settings'} component={Settings}/>
                        <Route exact path={'/changePassword'} component={ChangePassword}/>
                        <Route exact path={'/regEvent'} component={regEvent}/>

                        <Route exact path={'/temp'} component={TemporarySeat}/>
                        <Route exact path={'/tempPhoto'} component={TempPhoto}/>
                        <Route exact path={'/loader'} component={Loader}/>

                        <Route exact path={'/cabinet/admin'} component={AdminPage}/>
                    </Switch>
                </BrowserRouter>
            </div>
        );
    }
}

export default App;