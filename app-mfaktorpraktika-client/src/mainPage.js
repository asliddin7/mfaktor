import React, {Component} from 'react';
import axios from 'axios';
import './mainPage.css'
import Body2 from "./components/body2";
import Videos from "./components/videos";
import Footer from "./components/footer";
import Events from "./components/events";
import Speakers from "./components/speakers";
import Header from "./components/header";
import {toast, ToastContainer} from "react-toastify";


class MainPage extends Component {

    componentDidMount() {
        this.notifySuccess()
    }

    notifySuccess = () => toast.info("Platforma test rejimida ishlamoqda !!!");

    render() {
        return (
            <div className="main">
                <Header/>
                <div className="container">
                    <div className="row">
                        <ToastContainer />
                        <div className="body">
                            <Body2/>
                            <Videos/>
                            <Speakers/>
                        </div>
                    </div>
                </div>
                <Footer/>
            </div>
        );
    }
}

export default MainPage;