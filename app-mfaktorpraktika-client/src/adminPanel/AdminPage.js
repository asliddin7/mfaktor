import React, {Component} from 'react';
import '../style/adminPage.css'
import {Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";
import {AvField, AvForm} from 'availity-reactstrap-validation';
import {PATH} from "../util/utils";
import axios from "axios";

class AdminPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            events: [],
            showModal: false,
            modalShow: false,
            editModal: false,
            types: [],
            seats: [],
            speakers: [],
            edit: false,
            new: false,
            currentEvent: '',
            currentItem: '',
            deleteEvent: false,
            eventId: '',
            eventName: '',
            imagesId: ''
        }
    }

    componentDidMount() {
        // axios.get(PATH+'api/auth/me', {headers: {"Authorization": localStorage.getItem(TOKEN)}}).then(res => {
        //     console.log(res.data);
        //     if (res.statusCode!=="409") {
        //             this.setState({redirect: true});
        //             this.props.history.push("/admin/page")
        //     }
        //     else {
        //         this.setState({redirect: true});
        //         this.props.history.push("/login")
        //     }
        // })
        this.getVideoTypes();
        this.getSpeakers();
        this.getEvents()
    }

    //        VIDEOS AXIOS      //

    saveVideos = (e, v) => {
        axios({
            url: PATH + 'api/video/saveVideo',
            method: 'post',
            data: v
        }).then((res) => {
        })
    };
    getVideoTypes = () => {
        axios.get(PATH + "api/video/video/types").then(res => {
            this.setState({
                types: res.data
            })
        })
    };

    //        EVENTS AXIOS     //

    saveOrEditEvent = (event, values, errors) => {
        if (this.state.imagesId){
            values = {...values, attachmentId: this.state.imagesId};
        }
        if (this.state.currentItem) {
            values = {...values, id: this.state.currentItem.id};
        }
        axios({
            url: 'http://localhost:8/api/event/saveEventOrEditEvent',
            method: 'post',
            data: values
        }).then((res) => {
            this.closeModal();
            this.getEvents()
        });
        this.setState({
            editModal: false
        })

    };
    handleEdit = (item) => {
        this.setState({
            currentItem: item,
            editModal: true
        })

    };

    getEvents = () => {
        axios({
            url: PATH + 'api/event/getEvent',
            method: 'get'
        }).then((res) => {
            this.setState({
                events: res.data
            });
        })
    };
    getSpeakers = () => {
        axios.get(PATH + 'api/speaker/getAllSpeakers').then(res => {
            let s = [];
            let tempArray = res.data;
            tempArray.map(item => {
                s.push({name: item.user.firstName, surname: item.user.lastName, id: item.id})
            });
            this.setState({
                speakers: s
            })
        })
    };
    deleteEventById = (id) => {
        {
            axios.delete(PATH + 'api/event/deleteEvent?eventId=' + id).then(res => {
                this.getEvents()
            })
        }
    };
    deleteEvent = () => {
        this.setState({
            deleteEvent: true
        })
    };
    noDelete = () => {
        this.setState({
            deleteEvent: false
        })
    };

    //                        //

    openModal = () => {
        this.setState({
            showModal: true
        })
    };
    closeModal = () => {
        this.setState({
            showModal: false,

        })
    };
    showModal = () => {
        this.setState({
            modalShow: true,
            currentItem:''
        })
    };
    hideModal = () => {
        this.setState({
            modalShow: false
        })
    };
    editHideModal = () => {
        this.setState({
            editModal: false
        })
    };
    changingNew = () => {
        this.setState({
            new: true,
            edit: false,
            modalShow: false,
            showModal: false
        })
    };
    changingEdit = () => {
        this.setState({
            new: false,
            edit: true,
            modalShow: false,
            showModal: false
        })
    };
    garbage = () => {
        this.setState({
            new: false,
            edit: false
        })
    };
    saveImage = (e) => {
        document.getElementById("selectedFileName").innerHTML = document.getElementById("input-file").value;
        let formData = new FormData();
        formData.append("request", e.target.files[0])
        axios({
            url: 'http://localhost:8/api/file/saveFile',
            method: 'post',
            data: formData,
            headers: {
                "Content-Type": "multipart/form-data"
            }
        }).then((res) => {
            this.setState({
                imagesId: res.data.object.id
            })
        })

    };

    render() {
        const {showModal, modalShow, currentItem, events, editModal, deleteEvent, eventName, eventId} = this.state;
        return (
            <>
                <hr className="hrs"/>
                <div className="container">
                    <div className="row pt-1">
                        <h1 id="admin" className="admin" style={{fontFamily: "cursive"}}>Admin {
                            this.state.new ?
                                ">>> create new event or new video" : null
                        }
                            {
                                this.state.edit ?
                                    ">>> edit event >>>" : null
                            }
                        </h1>
                        <button className="saveNewBtn butt" onClick={this.changingNew}>
                            Save New
                        </button>
                        <button className="editBtn butt" onClick={this.changingEdit}>
                            Edit Old
                        </button>
                        <img onClick={this.garbage} style={{width: 40, height: 40, marginLeft: 50}}
                             src={require("../images/trash-512.png")} alt="trash"/>
                    </div>
                </div>
                <hr className="hrs"/>
                {this.state.new ?
                    <div className="saveNew">
                        <Modal className="videoModal" isOpen={showModal}>
                            <ModalHeader>
                                <h1 className="ml-5" style={{
                                    fontFamily: "cursive",
                                    fontWeight: "bolder",
                                    textAlign: "center",
                                    marginTop: "15px"
                                }}>Add New Video</h1>
                            </ModalHeader>
                            <ModalBody>
                                <AvForm onValidSubmit={this.saveVideos}>
                                    <AvField style={{fontFamily: "cursive"}} className="inputss" name="link"
                                             placeholder="Enter video's link"/>
                                    <AvField type="select" style={{fontFamily: "cursive"}} className="inputss"
                                             name="type"
                                             placeholder="Enter video type">
                                        {this.state.types.map(item =>
                                            <option className="inputss" value={item.type}
                                                    label={item.type}>{item.type}</option>
                                        )}
                                    </AvField>
                                    <button type="submit" className="saveBtn butts" onClick={this.closeModal}>Save
                                    </button>
                                </AvForm>
                                <button className="closeBtn butts" onClick={this.closeModal}>Close</button>
                            </ModalBody>
                        </Modal>
                        <Modal className="eventModal" isOpen={modalShow}>
                            <ModalHeader>
                                <h1 className="ml-5" style={{
                                    fontFamily: "cursive",
                                    fontWeight: "bolder",
                                    textAlign: "center",
                                    marginTop: "15px"
                                }}>Add New Event</h1>
                            </ModalHeader>
                            <ModalBody>
                                <AvForm onValidSubmit={this.saveOrEditEvent}>
                                    <label className="upload-btn">
                                        <input className="input-files" type="file" id="input-file"
                                               onChange={this.saveImage}/>
                                        <span id="selectedFileName" style={{fontFamily: "cursive"}}>Choose Image</span>
                                    </label>
                                    <AvField style={{fontFamily: "cursive"}} type="date" className="inputss"
                                             name="date"/>
                                    <AvField
                                        style={{fontFamily: "cursive"}} type="text" className="inputss" name="name"
                                        placeholder="Enter event's name"/>
                                    <AvField
                                        style={{fontFamily: "cursive"}} type="number" className="inputss"
                                        name="startPrice"
                                        placeholder="Enter start price"/>
                                    <AvField
                                        style={{fontFamily: "cursive"}} type="number" className="inputss"
                                        name="endPrice"
                                        placeholder="Enter end price"/>

                                    <AvField type="select"
                                             style={{fontFamily: "cursive"}} className="inputss"
                                             name="speakerId">
                                        {this.state.speakers.map(item =>
                                            <option className="inputss"
                                                    value={item.id}>{item.name} {item.surname}</option>
                                        )}
                                    </AvField>
                                    <button type="submit" className="saveBtn butts" onClick={this.hideModal}>Save
                                    </button>
                                </AvForm>
                                <button className="closeBtn butts" onClick={this.hideModal}>Close</button>
                            </ModalBody>
                        </Modal>
                        <div className="container">
                            <div className="row ffff">
                                <div className="col-md-12 mt-5 pt-3">
                                    <button className="btns editVideosButton" onClick={this.openModal}>New Video
                                    </button>
                                    <button className="btns editEventsButton" onClick={this.showModal}>New event
                                    </button>
                                </div>
                            </div>
                            <div className="row mt-5">

                            </div>
                        </div>
                    </div>
                    : null
                }
                <>
                    <div className="editOld">
                        <Modal className="eventModalEdit" isOpen={editModal}>
                            <ModalHeader>
                                <h1 className="ml-5" style={{
                                    fontFamily: "cursive",
                                    fontWeight: "bolder",
                                    textAlign: "center",
                                    marginTop: "15px"
                                }}>Edit Event</h1>
                            </ModalHeader>
                            <ModalBody>
                                <AvForm onValidSubmit={this.saveOrEditEvent}>
                                    <label className="upload-btn">
                                        <input className="input-files" type="file" id="input-file"
                                               onChange={this.saveImage}/>
                                        <span id="selectedFileName" style={{fontFamily: "cursive"}}>Choose Image</span>
                                    </label>
                                    <AvField defaultValue={currentItem && currentItem.date}
                                             style={{fontFamily: "cursive"}} type="date"
                                             className="inputss"
                                             name="date"/>
                                    <AvField defaultValue={currentItem && currentItem.name}
                                             style={{fontFamily: "cursive"}} type="text"
                                             className="inputss"
                                             name="name"
                                             placeholder="Enter event's name"/>
                                    <AvField defaultValue={currentItem && currentItem.startPrice}
                                             style={{fontFamily: "cursive"}} type="number"
                                             className="inputss"
                                             name="startPrice"
                                             placeholder="Enter start price"/>
                                    <AvField defaultValue={currentItem && currentItem.endPrice}
                                             style={{fontFamily: "cursive"}} type="number"
                                             className="inputss"
                                             name="endPrice"
                                             placeholder="Enter end price"/>

                                    <AvField type="select"
                                             style={{
                                                 fontFamily: "cursive",
                                                 textAlign: "center"
                                             }}
                                             className="inputss"
                                             name="speakerId">
                                        {this.state.speakers.map(item =>
                                            <option className="inputss"
                                                    value={item.id}>{item.name} {item.surname}</option>
                                        )}
                                    </AvField>
                                    <button type="submit" className="saveBtn butts">Save
                                    </button>
                                </AvForm>

                                <button className="closeBtn butts" onClick={this.editHideModal}>Close</button>
                            </ModalBody>
                        </Modal>
                    </div>
                    {this.state.edit ?
                        <div className="container">
                            <div className="row">
                                <div className="card-group mt-5">
                                    {
                                        events.map((item) => {
                                            return (
                                                <div className="card eventCard">
                                                    <div className="card-header">
                                                        <h2 style={{
                                                            textAlign: "center",
                                                            paddingTop: 10,
                                                            marginBottom: 15
                                                        }}>Event</h2>
                                                    </div>
                                                    <div className="card-body">
                                                        <h3 className="textEvent">Date: <i>{item.date ? item.date.substring(0, 10) : null}</i>
                                                        </h3>
                                                        <h3 className="textEvent">Name: <i>"{item.name}"</i></h3>
                                                        <h3 className="textEvent">Start Price: <i>{item.startPrice}</i>
                                                        </h3>
                                                        <h3 className="textEvent">End Price: <i>{item.endPrice}</i></h3>
                                                        <h3 className="textEvent">Speaker: <i>" {item.speaker.user.firstName} {item.speaker.user.lastName} "</i></h3>
                                                        <button className="butttt" style={{fontFamily: "cursive"}}
                                                                onClick={() => {
                                                                    this.deleteEvent();
                                                                    {
                                                                        this.setState({
                                                                            eventId: item.id,
                                                                            eventName: item.name
                                                                        })
                                                                    }
                                                                }}
                                                        >Delete
                                                        </button>
                                                        <button style={{fontFamily: "cursive"}} className="buttt mt-3"
                                                                onClick={() =>
                                                                    this.handleEdit(item)
                                                                }>Edit
                                                        </button>
                                                    </div>
                                                </div>
                                            )
                                        })
                                    }
                                </div>
                            </div>
                        </div>
                        : null
                    }
                </>
                <Modal isOpen={deleteEvent} className="deleteModal">
                    <ModalBody>
                        <h3 className="pt-3" style={{fontFamily: "cursive"}}>Want you delete "<i>{eventName}</i>" event?
                        </h3>
                        <p>🙄🙄</p>
                    </ModalBody>
                    <ModalFooter>
                        <button style={{fontFamily: "cursive"}} className="yes deleteBtn" onClick={() => {
                            this.deleteEventById(eventId);
                            this.noDelete()
                        }}>Yes
                        </button>
                        <button style={{fontFamily: "cursive"}} className="no deleteBtn" onClick={this.noDelete}>No
                        </button>
                    </ModalFooter>
                </Modal>
            </>
        );
    }
}

export default AdminPage;